## [3.0.6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.5...v3.0.6) (2024-12-24)


### Bug Fixes

* **update:** 添加项目字段[[#22](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/22)] ([a6b6560](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a6b65609f0bb8e0e0620d219876b718df6f44c70))


### Features

* **table:** 资源表创建视图支持自定义主键 ([b81d2ee](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b81d2ee19ef3046f6b84587e881810be6455b16e))



## [3.0.5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.4...v3.0.5) (2024-10-30)


### Bug Fixes

* **codes:** 适配全局代码资源 ([1395739](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1395739b72826c289a89d438dde317dfe3432d85))
* **doc:** 更新微应用使用手册文档 ([80674aa](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/80674aab38a9fbe1321d5e88ffc9f88b66478267))
* **login:** 登录插件传递的参数赋予globalStore的login事件 ([9364f92](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/9364f923e04cd415050ba439246e3ee348fb62c7))
* **login:** 更新主应用登录事件login改为admin-login ([414f67c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/414f67c4cf0920e9309b032ac1edba4b6e6d4c09))
* **micro:** 废弃微应用setupAdmin，交由主应用处理 ([27291e2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/27291e2756766bf4b396c3e21a7e95d2aa0fce62))
* **micro:** 减轻微应用逻辑处理，交由主应用负责 ([31ceba0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/31ceba0770b0f96fa0c27337b200994b879dd925))
* **micro:** 延时事件队列支持数组存储 ([9e9e4e7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/9e9e4e7219e18aebfc7541d17f62cae8d0c27e06))
* **micro:** 优化微应用加载时机，配合主应用使用 ([fd17bd4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/fd17bd4b903daf1077a91e28513c1c3afd47f589))


### Features

* **micro:** micro-store新增emitPromise方法，解决事件调用的时机问题 ([8109bc0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/8109bc0f96b3a6bfee6c904dd1ebeb038107de25))



## [3.0.4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.3...v3.0.4) (2024-09-19)


### Bug Fixes

* **build:** 修复构建项目错误 ([1ca15d0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1ca15d02ec68bf090af6b79d03b81aab7bb3f420))
* **build:** 增加基础微应用的地址代理 ([be06d28](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/be06d28ae46a8a4f61d0eec923c6efe4a6e4c84e))
* **main:** 调整入口文件的引入方式 ([648acc8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/648acc80112c9776860e0fb36d163df2aa060a1a))


### Features

* **build:** 新增env变量，支持配置主题变量文件和自定义引用别名 ([c1ed4c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c1ed4c1cb53b98c00e989b16e1ba55faf48afd29))
* **table:** 部门辅助可选择字典 ([0a8ec8c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/0a8ec8c039df6d6745af04c19029a05bfaee1ca9))
* **table:** 资源表部门辅助默认值修改 ([7a30171](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7a30171b353d79d1a0e474e6e2e18bc16aa51d98))



## [3.0.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.2...v3.0.3) (2024-07-26)


### Bug Fixes

* **icon:** 修复全局图标库加载问题[[#12](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/12)] ([7cc204a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7cc204a391b9d67f196caae01ed5c78ece505c39))


### Features

* **cli:** 去掉页面的语音 ([ad2f6fa](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/ad2f6fa211ffa60a2d3d7b77ac5bc589263ef042))
* **cli:** 页面调整为中文 ([3d16254](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/3d1625490f47b7eaea6e1ad0e8352b25c6df4448))
* **table:** 列表搜索时重置[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([2bafba8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2bafba842a2acbf4e404625eea1b21f23340fc2d))



## [3.0.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.1...v3.0.2) (2024-06-25)


### Bug Fixes

* **style:** 调整全局样式加载策略 ([8475ebd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/8475ebdb525e688496db5d4136d2a11805c381d9))



## [3.0.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v3.0.0...v3.0.1) (2024-06-01)


### Bug Fixes

* **mxgraph:** 删除mxgraph静态资源 ([90e9f78](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/90e9f788e48985538ff609085f935169b9daae52))
* **router:** 调整路由注册时机 ([699dbc0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/699dbc0ed38a9670b0563e611ddf96dda542c07f))
* **router:** 调整路由注册时机 ([a925766](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a925766664a5a78adbd2e5fcc1944545888db0eb))


### Features

* **version:** release v3.0.0 ([f5c0dea](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f5c0dea6ae917590dbb9e657929bda4f5ac210bc))



# [3.0.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.2.3...v3.0.0) (2024-04-25)


### Bug Fixes

* **builder:** 更新@jecloud/builder版本号 ([7696ee2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7696ee2838f2c5257a75ca9f5ff59a329fc3f59f))
* **builder:** 适配构架工具的依赖 ([29b285b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/29b285bddd2948aada11a7c3066f5fe59c3a318a))
* **interceptor:** 增加全局拦截器支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([e5d7dc6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/e5d7dc6610f8002f83b4f6c772f6d40cc3efbe57))
* **micro:** 优化微应用适配代码 ([8cfd1fc](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/8cfd1fc52c0559911736873f19d114c7175d1fd8))
* **package:** 更新@jecloud/builder ([374cd39](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/374cd3972aec369fc00958e1a72d613e9bcd86c1))
* **package:** 更新package-lock文件 ([c6f86d9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c6f86d929bd7b9c5686bdfcff4306e67c824f7d9))
* **table:** 资源表模块删除修复 ([cb49438](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/cb49438096e7113503e09e7b9a9ea573e723c57e))


### Features

* **lifecycle:** 增加系统生命周期文件，服务业务处理，main.js禁止修改 ([d59eb21](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d59eb21d511f62ce242f697dd2d495638d5b1437))
* **plugin:** 增加插件访问权限[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([2d2786d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2d2786d56a65ed5a835a9df981fa572208343f6c))
* **table:** 表格列操作前校验列表是否修改[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([3044aad](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/3044aad332666f0a4e9dfb042e27f5c67119d82c))
* **table:** 去掉操作表字段 ([ae7bbd6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/ae7bbd6eea00387030496fda95dfa2768969fa5f))
* **table:** sql视图查询列修复[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([4902d3c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4902d3cb6441d4673e41f716bde1a237785f29b5))
* **version:** release v2.2.3 ([3790234](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/37902342acb8f260ee50cadcb772e16f6fd51257))



## [2.2.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.2.2...v2.2.3) (2024-02-23)


### Bug Fixes

* **build:** 修改编辑器配置 ([492e4f6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/492e4f6eeae3db3e97eeb6ea3048d6a171fd7eed))
* **login:** 修复登录接口加密后，导致参数异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([6a784e1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/6a784e1e7a3ba2d56683060317a5b228e291e2d3))


### Features

* **version:** release v2.2.1 ([923781d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/923781d54fefa65301512e28a76eadde893d110b))
* **version:** release v2.2.2 ([73fa96f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/73fa96fa9c11489edc4a48494558464b68cdb1e3))



## [2.2.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.2.1...v2.2.2) (2023-12-29)



## [2.2.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.2.0...v2.2.1) (2023-12-15)


### Bug Fixes

* **public:** 优化静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([9d1cedd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/9d1cedd12b451b0e12a4858aec932b3dfde0eea9))


### Features

* **flie:** 添加图片预览资源 ([9cb8d34](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/9cb8d3471501c37c9182aeaca06bda2ea021f5db))
* **version:** release v2.1.0 ([f5ca7b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f5ca7b9f44666d6509850aae42dbae53e9a9567f))
* **version:** release v2.1.1 ([6dc31ab](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/6dc31abb52c2ee4d69ba9e6b6d8bc1783de0b4a7))
* **version:** release v2.2.0 ([8d1a7a8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/8d1a7a8a8a4a3ccff04f887a890cb4868483680c))



# [2.2.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.1.1...v2.2.0) (2023-11-15)



## [2.1.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.1.0...v2.1.1) (2023-10-27)



# [2.1.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.9...v2.1.0) (2023-10-20)


### Bug Fixes

* **http:** 优化http请求的拦截器和退出登录的逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([61178f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/61178f33af0d27bbd0a7f1798e6e6eba8209810c))


### Features

* **version:** release v2.0.8 ([8970821](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/89708211d43becbdbff332f64005cf60c108b366))
* **version:** release v2.0.9 ([0fbfd4a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/0fbfd4add77a87758df961c9e95ef2e3687a307f))



## [2.0.9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.8...v2.0.9) (2023-09-22)



## [2.0.8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.7...v2.0.8) (2023-09-15)


### Bug Fixes

* **env:** 更新服务代理地址[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([25bafcb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/25bafcb0e894b9a126d1b7067e8ab7edf2e81cc5))


### Features

* **doc:** 文件changelog修改提交 ([0028b1a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/0028b1a8b43fc25201838cb86d94951dba5fe1ac))
* **script:** 增加setup:lib命令，提供非源码客户使用[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([a2911c9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a2911c9b761941d3af6f7e73b5e7fb91f8aff203))
* **version:** release v2.0.4 ([abfadad](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/abfadad80aac3a9f264dff1c2c861d9dbcc0ffc8))
* **version:** release v2.0.5 ([14f06f4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/14f06f42d697b2fcd4132d72e9348059593347d5))
* **version:** release v2.0.6 ([75f479d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/75f479d97a391a8e43078bbae15e0d82c21b69a5))
* **version:** release v2.0.6 ([0e84c35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/0e84c358a52e1ca7df1040f58dfad18348faaa56))
* **version:** release v2.0.7 ([ca2a996](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/ca2a9961629a05cd7e9f4e8978e3446a558ce3e3))



## [2.0.7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.6...v2.0.7) (2023-09-08)



## [2.0.6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.5...v2.0.6) (2023-09-01)



## [2.0.5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.4...v2.0.5) (2023-08-25)



## [2.0.4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v2.0.3...v2.0.4) (2023-08-18)


### Bug Fixes

* **table:** 表辅助表名相同后缀字段错乱修复[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([7b62861](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7b62861866fbf58d02b160aea37007510df8c051))



## [2.0.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v1.4.0...v2.0.3) (2023-08-12)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 修复websocket代理地址失效问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([6975273](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/69752730079a6686f1be71e01f454f4f24f07609))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **security:** 删除密级管理无用参数 ([c4c748c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c4c748c12feeb9a84590f5ff85234584bfd05368))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **security:** 增加资源表添加密级字段菜单 ([44922b5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/44922b51d349a62c10f4efdc21b9e6ff9f1bf1f0))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([2482c52](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2482c52c2b1d18d1a74145e5c7bd72fa21ea582d))
* **table:** 资源表保存表格列数据同步关键字搜索[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([1a74fa5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1a74fa5048947e3bbb35357b772b5082d552df5b))
* **table:** 资源表表格列数据字典读取数据库[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([7b456cd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7b456cd3dd0499d499f3458441005cf098b2fc27))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([392b119](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/392b1193b1703a137503fd91fb4213565c4d073f))
* **version:** release v2.0.0 ([559c61f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/559c61f50396a83a4836d7e115341586caabfba2))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([7e61dab](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7e61dab0a97748c46643a329f7772eb5dba348c1))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([75cd9f9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/75cd9f9ed53ac2684d5fd8e157980bccfdc8d1b9))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))
* **version:** release v2.0.3 ([1793e6b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1793e6b39724654d98563036fade36e4282d8b14))



## [2.0.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v1.4.0...v2.0.2) (2023-08-04)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **security:** 删除密级管理无用参数 ([c4c748c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c4c748c12feeb9a84590f5ff85234584bfd05368))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **security:** 增加资源表添加密级字段菜单 ([44922b5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/44922b51d349a62c10f4efdc21b9e6ff9f1bf1f0))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([2482c52](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2482c52c2b1d18d1a74145e5c7bd72fa21ea582d))
* **table:** 资源表保存表格列数据同步关键字搜索[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([1a74fa5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1a74fa5048947e3bbb35357b772b5082d552df5b))
* **table:** 资源表表格列数据字典读取数据库[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([7b456cd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7b456cd3dd0499d499f3458441005cf098b2fc27))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([392b119](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/392b1193b1703a137503fd91fb4213565c4d073f))
* **version:** release v2.0.0 ([559c61f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/559c61f50396a83a4836d7e115341586caabfba2))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([7e61dab](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7e61dab0a97748c46643a329f7772eb5dba348c1))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))



## [2.0.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v1.4.0...v2.0.1) (2023-07-30)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **security:** 删除密级管理无用参数 ([c4c748c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c4c748c12feeb9a84590f5ff85234584bfd05368))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **security:** 增加资源表添加密级字段菜单 ([44922b5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/44922b51d349a62c10f4efdc21b9e6ff9f1bf1f0))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([2482c52](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2482c52c2b1d18d1a74145e5c7bd72fa21ea582d))
* **table:** 资源表保存表格列数据同步关键字搜索[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([1a74fa5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1a74fa5048947e3bbb35357b772b5082d552df5b))
* **table:** 资源表表格列数据字典读取数据库[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([7b456cd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7b456cd3dd0499d499f3458441005cf098b2fc27))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([392b119](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/392b1193b1703a137503fd91fb4213565c4d073f))
* **version:** release v2.0.0 ([559c61f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/559c61f50396a83a4836d7e115341586caabfba2))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d9b316b2671fb760e1456907c216adff855d4537))



# [2.0.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/compare/v1.4.0...v2.0.0) (2023-07-22)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **security:** 删除密级管理无用参数 ([c4c748c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c4c748c12feeb9a84590f5ff85234584bfd05368))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/56902100a501e29589a635f8d6b9b0f3e598e053))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **security:** 增加资源表添加密级字段菜单 ([44922b5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/44922b51d349a62c10f4efdc21b9e6ff9f1bf1f0))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([2482c52](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2482c52c2b1d18d1a74145e5c7bd72fa21ea582d))
* **table:** 资源表保存表格列数据同步关键字搜索[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([1a74fa5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/1a74fa5048947e3bbb35357b772b5082d552df5b))
* **table:** 资源表表格列数据字典读取数据库[[#18](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/18)] ([7b456cd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/7b456cd3dd0499d499f3458441005cf098b2fc27))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([392b119](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/392b1193b1703a137503fd91fb4213565c4d073f))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-table/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.4.0...v1.4.1) (2023-06-30)


### Bug Fixes

* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **security:** 删除密级管理无用参数 ([c4c748c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c4c748c12feeb9a84590f5ff85234584bfd05368))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))


### Features

* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **security:** 增加资源表添加密级字段菜单 ([44922b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/44922b51d349a62c10f4efdc21b9e6ff9f1bf1f0))
* **table:** 资源表表格列数据字典读取数据库[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([7b456cd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7b456cd3dd0499d499f3458441005cf098b2fc27))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))



# [1.4.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.2.0...v1.4.0) (2023-06-06)


### Bug Fixes

* **ajax:** 修复主子应用共用ajax实例[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([94f40c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/94f40c21f97664e3f0150830f1e6e93ed1849aa9))
* **plugin:** 废弃@jecloud/plugin,调整JE的注册[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([052a918](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/052a918eec2e9cdab556e52fbecc727384ebecd4))
* **update:**  应用中心打开资源表数据回显[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([26fbfd5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/26fbfd5b0101ebcbd0735f90ffe24dd8302ab22f))
* **update:**  资源表添加[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([84338f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/84338f18259dbe0cffc4aefdfeb3ccf863a6e75f))
* **update:**  资源表添加[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([cf2cb89](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cf2cb89ec62a9ba45755c6e875e5babe2d4b66d2))
* **update:** 参数修复[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([031fb01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/031fb01756f079badeb923fe05876a6eb4e8e017))
* **update:** 传参获取对应的row[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([ca9cdfa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ca9cdfaa16d9693b6535be3e81895af302e87d48))
* **update:** 从不同入口进入到资源表处理[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([466ef83](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/466ef832cac5d313bd991a6a7541b8710ad33961))
* **update:** 打开应用中心的地址增加方案[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([e5bb78a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e5bb78aa03d83feae3e23e70c52f85f49864116d))
* **update:** 打开资源表传参数据修复[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([22b9f8a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/22b9f8a9b10eaf794ba77b7f6c8baf5af27757bb))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([a27d160](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a27d16015a2254f846c70324ff1f30a716d0ed20))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([139cbad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/139cbad2aca960d48d824f3796923905ebe44481))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([36c0ca4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/36c0ca43164518eedd7588ce7428311baf989160))
* **update:** 获取参数调整，queryTODO[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([9f58469](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9f58469d2daf5157917fa1aa93ba1271ee50f1b1))
* **update:** 解决语法报错问题[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([c17645c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c17645ceda14e2c3bdf51ad3a255030f02ddd613))
* **update:** 解决资源表只要一刷新就是元数据服务,每次都是要切换[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([f17a63b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f17a63bf65809f572918fb8090c657a3a9ed930d))
* **update:** 解析地址的时候要进行解码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([ac505f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ac505f05264fc3675ae51548db4ffe5fe7b7c6fe))
* **update:** 首页进入延迟调取接口[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([c7d95ab](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c7d95ab8013e6f7d446df250ca6a7cb503c37270))
* **update:** 添加子系统打开资源表逻辑[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([67ff88d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/67ff88d4783586d3a27e6c227941691ec05b24a2))
* **update:** 优化代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([d8f821a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d8f821af28cefb2a78d294b8ba9271e61479daba))
* **update:** 资源表产品联系上下文做服务的处理[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([59e8fb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/59e8fb29d656684eb7c2142a060ba00149bcdee8))
* **update:** 资源表打开应用中心功能[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([10a7b39](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/10a7b393eecfe03106b74c2db2a76953df7e3d56))
* **utils:** @jecloud/utils删除vue依赖[[#67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/67)] ([e6929e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e6929e0d0df43b58c15f3bdd41d20d525f30fa20))
* **utils:** 适配jecloud/utils的调整 ([47b8cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/47b8cdd79cfd1d2a459f809a5ebbf8bcd11ce8e5))


### Features

* **admin:** 打开菜单暴露的方法参数修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([2bccf20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2bccf2032a234a7797ebdcff58da6dd2baa60a0b))
* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 图片预览区分节点加载[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([2876a2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2876a2eed3a5e13047094d4034aff39e88bebfcf))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/affc489c088b3b77dbd2d176a887562726735540))
* **je:** 暴露JE常用方法 ([4efb75b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4efb75b4881eae4000153638e1d0d4b4628547cb))
* **je:** 将vue的h函数添加到JE上 ([753fad9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/753fad92b4fbf4a7aabbcc29fdd3b950ad3f426a))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **table:** 按钮图标更换[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([536f2c3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/536f2c31cabb8e9ce324fb0d86fa86263a90453f))
* **table:** 表格列保存逻辑修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2ae5c21](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2ae5c21f25855e7f2ed994f9e23360d16c3b4352))
* **table:** 表格列字段添加类型[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([e5b2950](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e5b2950779488c67cfbc2d717fafe06bf46d359c))
* **table:** 表删除逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2459221](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/245922180888ac529c97724c7fedf60dba347bab))
* **table:** 导出表修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6e9d791](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6e9d79109bdb810ccad9b305d1f65c2833f299c0))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([aa02054](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aa0205450547fb187fbab6e0534ae02b82d57764))
* **table:** 面板SQL视图搜索条数加正则校验[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a5f220f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a5f220f2e01dd72f79ac0733b198438452839ceb))
* **table:** 清空缓存接口加pd[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f7857f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f7857f087b1728afdd80a15dd512feaeda4a9bcf))
* **table:** 去掉代码编辑器左侧的预览图[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6f6134b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6f6134b82ff8633a53928f2ab64f64f48bc8ecf9))
* **table:** 去掉select组件[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a5b4018](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a5b40188d9342ea74b94e356648bfd35d343f755))
* **table:** 人员辅助/表辅助弹窗样式修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([1ad026c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1ad026ccbf98220454a2061cf8da92267d945068))
* **table:** 如果删除的字段有表格键关联关系,需刷新列表[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f068536](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f068536e12369903766501be4d0c387302947447))
* **table:** 设计视图里面切换表格键和表索引tab页刷新列表数据[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([3ff4cd6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3ff4cd6c34c1b798094b3b203861039dc6c3938c))
* **table:** 设计视图生成DDL弹窗修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([1ed1470](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1ed1470b561ddcde5933076f4a36929d1e0b611f))
* **table:** 设计视图生成DDL面板样式修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([4eabf09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4eabf0959638139367f2299172c2cc6176836145))
* **table:** 设计视图输出视图页面修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([c870e01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c870e013f583fdc8617582b206291680202ac7ea))
* **table:** 设计视图添加字段修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([b38dddd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b38dddd94c8704bd56282f54dd88da8c74f2c340))
* **table:** 设计视图修改代码编辑器配置[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([63966c8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/63966c87cb6dc5f5114f58aac9746a57f0454502))
* **table:** 设计视图中的输出视图搜索逻辑修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([e63966d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e63966db7abd7e1732c7d9527b9d60b4b5f25c13))
* **table:** 设计视图自然语言翻译添加接口[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([b525912](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b5259126173da44526084c5683b49750514f911a))
* **table:** 视图关系设置字段可选可编辑[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([b9e0087](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b9e0087364d530d46fe9a16bf3c7838d0acd2082))
* **table:** 视图划线逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([0f2cd2f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0f2cd2f98a577f7fd6e6029b7a744be59561453a))
* **table:** 视图逻辑修改#[18] ([411fc4b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/411fc4bb812db60c6b5ff00f70f824b791e23b24))
* **table:** 视图设置页面设计视图逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([59d3c13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/59d3c13647bc364f2e09145db37dd8246adb318a))
* **table:** 视图生成ddl面板提交[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([9542195](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/95421951cfc4bc6995bb8ecde1086e90355f4d86))
* **table:** 视图输出视图类型和长度变为可编辑[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([774c892](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/774c892e12c860d9f00222310ab4ecc8f5e81ba5))
* **table:** 视图输出视图列表条件值加提示弹窗[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([25de338](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/25de3388ccdf19258737cf489c95cec6e44bd138))
* **table:** 视图输出视图搜索修复[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([27ce496](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/27ce49603b733d472e8a0a135845a433ddcb430f))
* **table:** 视图添加未选搜索[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2bd0d5d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2bd0d5dd3da2a2147e65ec0fc0a62ed17c0ee58d))
* **table:** 视图选择主键时优化页面样式以及逻辑[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([ca6f1fe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ca6f1fe0c42680b426382633db6db5491309cf41))
* **table:** 提交packagelock文件[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([fee73d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fee73d92905353da3728b7f25d0508e2fdc495ed))
* **table:** 添加打开数据字典功能的按钮[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([76af3ac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/76af3ac25c0460c38de21df52f099c1d6c863173))
* **table:** 添加复制查询sql按钮[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([645e396](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/645e39607b899e0d99fa78c46849f675515ddc5b))
* **table:** 增加打开应用中心按钮 ([8069ffa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8069ffa3810ead9afe8ca00c454e943fb7832be2))
* **table:** 资源表操作视图值可编辑[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([884d305](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/884d305e2c72effbf9d4e211b8c62401ba9d3995))
* **table:** 资源表导出接口修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([988fdf4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/988fdf46f305daf60584d2d222d2ba569cb35ea9))
* **table:** 资源表逻辑修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([c2ccf83](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c2ccf83515e2c0325050e5943a67865df129cc3f))
* **table:** 资源表设计视图只读配置[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([0358b3e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0358b3e97cb62584681f109e144e0b3693a5a3e3))
* **table:** 资源表设计视图中输出视图添加字段逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a47dcfc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a47dcfc2f2ab424837c4ddd99347a3555126501f))
* **table:** 资源表设计视图ddl面板高度计算优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6b3b77f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b3b77f594ed83c1df7304a095bb8a2c6b6e1f68))
* **table:** 资源表升级[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([b37d720](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b37d720a427b76dbb5d5020a1c15f3e4b2792b26))
* **table:** 资源表视图搜索优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([16d719e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/16d719e457696dd7c85362d7fa255ac0ecf3954d))
* **table:** 资源表添加表格列逻辑修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2a4a309](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2a4a3099fc3fad1ac2d4d61de5a488526fee67e6))
* **table:** 资源表移动成功后加提示语[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a4be72e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a4be72e214181e527d59db2d229fe81cdcfe5a9d))
* **table:** 资源表优化提交[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([bdd15fe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bdd15fed4d8aa13aec5fc3e1ec42d203d888ad1c))
* **table:** 资源表优化修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([99beeb7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/99beeb7e50856a32767da600632e4f0c4e858984))
* **table:** 资源表字段命名规则修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([545fdf4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/545fdf48bba2e7b79234f9a86998176e206cee77))
* **table:** 资源表sql面板判空 ([36b5869](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/36b586944af959c857c4fcdac10dd61eaf061862))
* **table:** 资源表sql视图查询字段顺序[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([57bb030](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/57bb030ac0d10f436a48a4794258d3e62d8c1897))
* **table:** 资源表sql视图复制sql语句逻辑修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f5ca1de](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f5ca1dea46c11cf35899582e9c9ab9ec57fb2623))
* **table:** 资源表sql视图搜索优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([0a30bd5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0a30bd58d57ee4348a6e3a219f4af48b50434ed1))
* **table:** 字段命名规范逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([8b97ca2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8b97ca2fb2f2742650e8c0b79fe78da9939d170a))
* **table:** ddl视图中的代码编辑器只读[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([0a5dc4d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0a5dc4de0e797bae26f3429a6e6fb575bca8d6ef))
* **table:** sql展示加主题色 ([b015c3c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b015c3c917b8a2e903cd118a9251f3b7552b36b9))
* **version:** release v1.3.0 ([24c3abe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/24c3abe2f9d78183ba778556d3256e9112d433ba))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.3.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.2.0...v1.3.0) (2023-05-05)


### Bug Fixes

* **update:**  应用中心打开资源表数据回显[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([26fbfd5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/26fbfd5b0101ebcbd0735f90ffe24dd8302ab22f))
* **update:**  资源表添加[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([84338f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/84338f18259dbe0cffc4aefdfeb3ccf863a6e75f))
* **update:**  资源表添加[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([cf2cb89](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cf2cb89ec62a9ba45755c6e875e5babe2d4b66d2))
* **update:** 参数修复[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([031fb01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/031fb01756f079badeb923fe05876a6eb4e8e017))
* **update:** 传参获取对应的row[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([ca9cdfa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ca9cdfaa16d9693b6535be3e81895af302e87d48))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([a27d160](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a27d16015a2254f846c70324ff1f30a716d0ed20))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([139cbad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/139cbad2aca960d48d824f3796923905ebe44481))
* **update:** 调试代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([36c0ca4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/36c0ca43164518eedd7588ce7428311baf989160))
* **update:** 获取参数调整，queryTODO[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([9f58469](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9f58469d2daf5157917fa1aa93ba1271ee50f1b1))
* **update:** 解决语法报错问题[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([c17645c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c17645ceda14e2c3bdf51ad3a255030f02ddd613))
* **update:** 解决资源表只要一刷新就是元数据服务,每次都是要切换[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([f17a63b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f17a63bf65809f572918fb8090c657a3a9ed930d))
* **update:** 解析地址的时候要进行解码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([ac505f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ac505f05264fc3675ae51548db4ffe5fe7b7c6fe))
* **update:** 首页进入延迟调取接口[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([c7d95ab](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c7d95ab8013e6f7d446df250ca6a7cb503c37270))
* **update:** 添加子系统打开资源表逻辑[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([67ff88d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/67ff88d4783586d3a27e6c227941691ec05b24a2))
* **update:** 优化代码[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([d8f821a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d8f821af28cefb2a78d294b8ba9271e61479daba))


### Features

* **admin:** 打开菜单暴露的方法参数修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([2bccf20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2bccf2032a234a7797ebdcff58da6dd2baa60a0b))
* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/affc489c088b3b77dbd2d176a887562726735540))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **table:** 按钮图标更换[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([536f2c3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/536f2c31cabb8e9ce324fb0d86fa86263a90453f))
* **table:** 表格列字段添加类型[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([e5b2950](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e5b2950779488c67cfbc2d717fafe06bf46d359c))
* **table:** 关系视图跳转菜单[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([aa02054](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aa0205450547fb187fbab6e0534ae02b82d57764))
* **table:** 面板SQL视图搜索条数加正则校验[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a5f220f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a5f220f2e01dd72f79ac0733b198438452839ceb))
* **table:** 清空缓存接口加pd[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f7857f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f7857f087b1728afdd80a15dd512feaeda4a9bcf))
* **table:** 设计视图里面切换表格键和表索引tab页刷新列表数据[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([3ff4cd6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3ff4cd6c34c1b798094b3b203861039dc6c3938c))
* **table:** 添加打开数据字典功能的按钮[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([76af3ac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/76af3ac25c0460c38de21df52f099c1d6c863173))
* **table:** 增加打开应用中心按钮 ([8069ffa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8069ffa3810ead9afe8ca00c454e943fb7832be2))
* **table:** 资源表优化修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([99beeb7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/99beeb7e50856a32767da600632e4f0c4e858984))
* **table:** 资源表sql面板判空 ([36b5869](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/36b586944af959c857c4fcdac10dd61eaf061862))
* **table:** sql展示加主题色 ([b015c3c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b015c3c917b8a2e903cd118a9251f3b7552b36b9))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.2.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.0.2...v1.2.0) (2023-04-06)


### Bug Fixes

* **build:** 修复样式文件采用参数引用方式 ([dc6fd3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/dc6fd3fecc0c02b536590915888f967333cdce44))
* **font:** 修复字体文件为版本参数引用 ([a353a02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a353a02d0cf04447a92ae635604b4545a6a18f63))
* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))


### Features

* **cli:** changelog提交 ([0a4c001](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0a4c001c14276bacd26ff9dd3b5798fab203a459))
* **cli:** packagelock文件提交 ([9282afd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9282afd0bc17b871511e3d59bf7f9f2fee378440))
* **cli:** v1.1.0定版 ([11af62b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/11af62bd47230ee6d422effa56e4dd446b0ecca0))
* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **pdf:** pdf预览资源 ([530c87e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/530c87e6eea990882732446b3ca3a8f74310968a))
* **table:** 导入表字段添加正则修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([28e9fc1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/28e9fc140d609cfd4aae31a25f1e80ad928d1e28))
* **table:** 去掉组件生成策略的列[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2d9e811](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2d9e811e69522b44c777ff1486822df9564db343))
* **table:** 删除外键刷新左侧树[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([4120b9f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4120b9ff24559eb6bed7581133db4698f252cd71))
* **table:** 视图跟换字段不可以输入[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f65f884](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f65f8843ef766dc46a94d8dec7c1d85bbdf2aa5b))
* **table:** 视图也可以修改主键[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([bdb364f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bdb364f0b093ba6a62b5fdc0c17f89def11c8627))
* **table:** 树形表没有主键生成策略按钮[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([97ff4e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/97ff4e8a4ecb3f90380661079ee231072606449a))
* **table:** changelog提交 ([17a0ba6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/17a0ba6fe05d5f0eefc39f58074eeda191f746f4))
* **table:** changelog提交 ([5337e6f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5337e6fcee28c868643acf407158fa8673117053))
* **table:** v1.1.0定版 ([aa8d73f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aa8d73f328c377e225cc5a3d29447155433c046d))
* **version:** release v1.2.0 ([d0add8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d0add8b04191d33377beb32b36df2c4cb29b87af))



# [1.1.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.0.2...v1.1.0) (2023-03-03)


### Bug Fixes

* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))


### Features

* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **table:** 导入表字段添加正则修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([28e9fc1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/28e9fc140d609cfd4aae31a25f1e80ad928d1e28))
* **table:** 去掉组件生成策略的列[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2d9e811](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2d9e811e69522b44c777ff1486822df9564db343))
* **table:** 删除外键刷新左侧树[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([4120b9f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4120b9ff24559eb6bed7581133db4698f252cd71))
* **table:** 视图跟换字段不可以输入[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f65f884](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f65f8843ef766dc46a94d8dec7c1d85bbdf2aa5b))
* **table:** 视图也可以修改主键[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([bdb364f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bdb364f0b093ba6a62b5fdc0c17f89def11c8627))
* **table:** 树形表没有主键生成策略按钮[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([97ff4e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/97ff4e8a4ecb3f90380661079ee231072606449a))
* **table:** changelog提交 ([5337e6f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5337e6fcee28c868643acf407158fa8673117053))
* **table:** v1.1.0定版 ([aa8d73f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aa8d73f328c377e225cc5a3d29447155433c046d))



## [1.0.2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/v1.0.0...v1.0.2) (2023-01-11)


### Bug Fixes

* **code:** 调整代码，适应所有分支结构 ([2e7d448](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2e7d44879680fda6cf2af72b7c6b1fd4cc18ed5c))
* **icon:** 升级图标配置 ([ae975c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ae975c4dec20dcb889971157953f7227826c56ab))
* **logout:** 修复退出登录接口[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([e244604](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e244604c6aa1bfd01268e4cbf35af8e7a82afc44))
* **logout:** 增加退出登录接口调用[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([bda04a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bda04a21469159c1973ab121b15c49c81f838425))
* **npm:** 废弃pnpm安装，采用原始npm安装依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([9e653ec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9e653ec168b9b0ce44aafb2f59c656ba1f2811cd))
* **npm:** 更新npm lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([11cbdc5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/11cbdc56dafbc7f0ada60ac8b0bf8716840ef493))
* **npm:** 修改npm私服地址 ([cb61aad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cb61aadb4077424573b8b7e8770972d0091aa569))
* **pnpmlock:** 文件提交[[#14](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/14)] ([19cd4b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/19cd4b56fcd2b22f0a40db3712fecff88e857b2f))


### Features

* **定版:** 1.0.1定版 ([cf380f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cf380f1defc169bb47228ed6d193077031e53129))
* **定版:** 1.01定版 ([8f03fe1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8f03fe15e8dd6d34ef029848a86bb0d7aa3d0931))
* **cli:** package-lock文件提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([9f37092](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9f370921f4dd15533377b8ec432d149bc5500ef8))
* **export:** 表导出修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([117e3dc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/117e3dc1da9893e2f9efdfb4b20a7a64ce89b9a7))
* **exportTable:** 导出表结构修复[[#14](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/14)] ([6a72d46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6a72d4629c00b6622feb33475d33865f97a8e71a))
* **locales:** 国际化方法修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([ef3aef4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ef3aef469c19a2a90ae28339a6ecd48175e80557))
* **modal:** 弹窗样式修改[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([7007ae7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7007ae726e1ce936da9710772c67380df4213f39))
* **modal:** 弹窗样式修改[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([a06a40b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a06a40bcccefe8d52c4fb00594e41ef4ec8dbb46))
* **tabelColumn:** 表格列字典字典改name,code不变[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([2baf267](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2baf267fcc8a06d901b72aa1d2b6eda17a3f17a4))
* **table:** 表导入数据过滤[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([72366b3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/72366b385dcb6abc8eb149f3b3e9f76d7c333dc0))
* **table:** 表辅助修改[[#15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/15)] ([f9c06ef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f9c06efb95b4165532ffe38ac836ece0b50f13b8))
* **table:** 表辅助修改[[#15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/15)] ([ffe7a07](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ffe7a07e1317c638d33a1de5d9e1180bc2ea9f50))
* **table:** 表辅助修改下拉框弹出位置[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f17c2e3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f17c2e3b1429a64d022cde733b94e02755e6426f))
* **table:** 表辅助修改字段[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([d5a0d9a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d5a0d9a96d253cc5c7f1fc9554836ba15156ab62))
* **table:** 表格键修改[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([b2efaa4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b2efaa4b7a107043b6e69087615995449dadefad))
* **table:** 代码提交[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6ea6229](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6ea6229a774e5f2bfb708fc698b7e3f311f336a2))
* **table:** 导出修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([60ca6f2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/60ca6f26b7f7d2a7bdddbe9971e0d1b1b167f0bf))
* **table:** 导出label修改[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([eba65ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/eba65ae8aa9c832c4908604b53ea04604396c88c))
* **table:** 历史留痕修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6a3eb50](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6a3eb50bbf0de6fd6f60621a75af64ddd7066666))
* **table:** 主键生成策略接口修改[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([2057886](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2057886c84f69291e1b16681d59922f29870a774))
* **table:** 资源表服务改为产品[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([c6633fa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c6633faaf31799f6cd372a19db3fb92f62f6a6a8))
* **table:** 资源表智能排版逻辑优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([f7165f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f7165f57bfe1edc527ae744e707b6e4604db4c4b))
* **table:** 资源表tab面板和左侧tree操作联动优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([ffd31ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ffd31ca7c58b9a032f5d37d552ebe7b773b6600c))
* **table:** 资源表tab面板切换于左侧树联动优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([94d928d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/94d928dfa279279b87cc07ea6f5f483c45275b50))
* **table:** 字段辅助添加字段传字典name[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/16)] ([66d8ad0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/66d8ad0e1f5f08d061ddbe35cba95f7c7e14c68d))
* **tableAssist:** 表辅助 主键和外键的code值不变[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([eb1cd91](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/eb1cd912fad2fb95d32a7b4d1853b37b4f0dd620))
* **tablecloumn:** 列表添加的字典字段name更改code不变[[#15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/15)] ([110aee7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/110aee7d277c48f1e0b6467286c1838d618362f6))
* **table:** packagelock文件提交[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([6680dd9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6680dd99f77ef8c06236f97ae3043f39a3cf60c9))
* **table:** sql执行查询优化[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/18)] ([a543e0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a543e0e08d20d846b5625b722cc9a294395e5df0))



# [1.0.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/compare/28659bb098e260eee3fec6522782483d5defdf0b...v1.0.0) (2022-08-30)


### Bug Fixes

* **admin:** 主子应用打通 ([7afb056](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7afb0562c91be650cd3fb38fb79e15868bc2506c))
* **ant:** 更新ant-design-vue@~3.1.0-rc ([4ca2bd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4ca2bd307e68b797e96d8a9269cdde02ba795435))
* **antd:** 调整vite依赖引用 ([74942d5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/74942d5abe0b9579966d859f542fc6bb9c264d81))
* **antd:** 暂注掉antd图标按需加载，后期调试 ([c5b4766](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c5b476687c631a14dee0540080843dd7a90092ec))
* **assets:** 支持动态加载微应用资源 ([a121b57](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a121b57a0fbf949d3f4efc44d989dcc3a1e95449))
* **axios:** 修复axios格式化数据 ([145baff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/145baff06da0a57e5b765b6ec85c21246923afae))
* **axios:** 修复axios格式化数据 ([1c98fbf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1c98fbf1e59027e16ef328dee9746e6b1f19eb82))
* **axios:** 增加axios微应用代理前缀prexyPrefix[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([76b6650](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/76b6650d43b09fbcb130bb4941729dec562497b0))
* **babel:** 修改按需加载引用目录为es ([38ac102](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/38ac102a89be1dc747856862090935197065fdb9))
* **base:** 根据开发规范修改文件命名[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([0ff9f79](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0ff9f7981dece0965145d1fc7f97c92c81c52a90))
* **base:** 根据开发规范修改文件命名[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d03badf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d03badf4aea253280b40eaf16e7fcd88208a3299))
* **bug:** 修复主题，登录等bug ([9116e9b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9116e9bb7524f8fe1b2f25870694c2f930baa002))
* **build:** 调整项目打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8a20fe0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8a20fe0fb4a0a3022dce708c5fe28ed9ebefbd5a))
* **build:** 去除sourceMap[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0f91481](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0f914813b0e422cd8a26fafb8c9cf006a9483907))
* **build:** 剔除monaco打包处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([01b92ac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/01b92acff392264ed061de55542104569796cba7))
* **build:** 提供公共资源的json配置文件，方便应用调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([3122798](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3122798d67192219d4cf2cc44b25c6f98d2845bd))
* **build:** 修复微应用打包后的数据文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([bc40d94](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bc40d94ba4547e7e047933cf44e9ecc042aac8a9))
* **build:** 修复env配置文件 ([0044821](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0044821ee5f3107e1998dbac970fdf46524a727b))
* **build:** 修改webpack打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([395061c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/395061c23f9612d753f930006957fa7c04e2ba98))
* **build:** 优化打包流程 ([3cf58a3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3cf58a361e80f2fb3f10d39c157bb26a9c6ec7e0))
* **build:** 增加图片资源打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([5ef32f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5ef32f6b8deadbc6a526daac23892e06366c82c7))
* **build:** 主子应用打包使用相同配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([63ed9d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/63ed9d34fd536af080e46e41c8b8b37e10af05ee))
* **code:** 调整代码结构 ([3ef4c2d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3ef4c2d69dde2428574985b7c5b3c68fa312824e))
* **css:** 统一调整antd个性化样式引入方式 ([d7d1bbd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d7d1bbdc3166a0de1982d75029428adae67de525))
* **doc:** 由于pnpm7不兼容yalc，不支持file:xxx安装本地包，所以请使用 pnpm6[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([c461e36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c461e36575353d9814b02e5360f08e1e562a3b25))
* **envs:** 修改系统变量 ([015b6a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/015b6a21b59a90e0cd7f89c531edf8085b231d06))
* **eslint:** 增加 'no-case-declarations': 'warn'[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([08e2b46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/08e2b46ade750da82458707642d4cb546e04c5db))
* **file:** 更新pnpm-lock文件 ([b7aed24](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b7aed246df4955c07883568d910d300a513aad67))
* **i18n:** 抽离监听国际化的方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([c4506b4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c4506b49d16f8e17f4629e1ddeee15c80226d035))
* **i18n:** 调整国际化，增加使用说明 ([f4b194b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f4b194be3f07e94541d40532aa59200df52e0127))
* **i18n:** 调整国际化，增加使用说明 ([65947d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/65947d92303588185715f8711fc58a56d4e96591))
* **i18n:** 调整i18n代码结构 ([26768ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/26768aee6fb6294c7462923af3a65c46b16c4c0d))
* **i18n:** 去除i18n打包配置，导致微应用异常[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0bd0cec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0bd0ceca303cbb87c460607bde6a96ad49b05ff1))
* **i18n:** 修复国际化问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([352b873](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/352b8734fa951dfabec3e611812f80f5d7c2dac1))
* **i18n:** 修复i18n旧模式下api兼容 ([450d2cf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/450d2cf1a1f23ca02c85b871e2cf4436c25fafbd))
* **icon:** 更新图标文件 ([732a444](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/732a44439fce9a759abd67bb82ff8bbc56a73592))
* **image:** 删除无用文件 ([28659bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/28659bb098e260eee3fec6522782483d5defdf0b))
* **index:** 修复初始化配置设置 ([64d3c8e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/64d3c8e813b53b99213fe23ddd0b3e08c2fa807d))
* **index:** 修复打包错误 ([cf96bfa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cf96bfa76a11751356fb44cea55b1edc412164d9))
* **je:** 调整je按需加载 ([6da9c79](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6da9c793339785e7f7dd7628224106fa0379e2ba))
* **layout:** 修改路由容器不支持滚动条 ([46a5b04](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/46a5b0413141d68e659cd32c63fa2c95f444fe04))
* **less:** 固定less版本3.0.4,防止主题构建有问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([03b82b6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/03b82b6cbeec0603824c19f9e2c81d3b76541408))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e0df5f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e0df5f61d16a63e1b7ac29dfbf4f53b931991be0))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f8809bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f8809bb47660553bad5a9e69c45f05c68c4b69da))
* **lodash:** 修复lodash工具包依赖 ([9dd17e2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9dd17e20446fa90b7503eed1fa4070c2b3d2c970))
* **lodash:** 增加lodash工具包 ([6c13199](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6c13199b0eae2484ba16d6fe4f1b6a5c6d246ad7))
* **login:** 抽离login方法，提供全局调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([729a2c5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/729a2c548229491a6a5242fb2798fcb6945f19a0))
* **login:** 微应用登录页丢失，默认使用系统登录[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([15735dd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/15735dd58155ee5441fcae9fc266693abc398714))
* **login:** 修复登录成功后，路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a66b4c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a66b4c0323c788c752ced8d837a83c891023b601))
* **login:** 修复登录后路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2ea0c62](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2ea0c62add33877bdc995bb27d9623019904c870))
* **login:** 修复登录相关操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([4747fcd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4747fcd3b915c28606fc070649db627b0ed0953c))
* **login:** 修复用户失效，退出登录 ([1fc2dfb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1fc2dfb81b62bd95e483b305f0c8ff3627a68a77))
* **login:** 修复用户失效，退出登录 ([2eed3ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2eed3ae3626937f7769284d7bf51897c8b41af1a))
* **login:** 修复主子应用交互混乱问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e42b4ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e42b4ffda48cd7afaaeb3bf3fb4f4a87331e8b14))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([39587c9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/39587c989a192aebfa2d42d224fd4c9c9a19968c))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([15cbd0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/15cbd0e5c1fea45fe97f874a11ab4ab733d628b1))
* **login:** 增加网站备案号 ([f8641f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f8641f71111a5cae697ca8398cd5f158e13f1142))
* **login:** 增加login样式 ([7a0db20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7a0db20513807519bfa04048074ffa3b639f1689))
* **logout:** 将logout从JE抽离到system ([abf67e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/abf67e05098cc8f85c478294ca8de9c34a875dd0))
* **main:** 修复入口文件引入[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([d42cbfd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d42cbfda1ef50edb0d0588e77175ef2cae7e7371))
* **main:** 修复入口文件引入[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([f389efc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f389efcf12c4b90d8160140136f02b14602bd724))
* **micro:** 调整微应用代码结构 ([91501cb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/91501cb6a6024d28583e712cf5da1e4d382c3f8f))
* **micro:** 设置登录后的系统信息[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8facb67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8facb67d32b666828fc06998ce44380a6694e8e9))
* **micro:** 设置子应用共享主应用系统数据[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ebc8432](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ebc843288f4d94d8946022ed7b2de4de07b5f283))
* **micro:** 修复子应用注册登录成功事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f542360](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f542360d7f67dc2e3b3ad49f56b84dffd4b54fa4))
* **micro:** 修复microStorebug[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/45)] ([16dda0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/16dda0effefd2b376e533ad44d9961ebcde43916))
* **micro:** 优化主子应用交互方式 ([e4b9be6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e4b9be68f5b458310b2f5db509a0ac65865c298b))
* **micro:** 优化子应用打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e3a93c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e3a93c248e3d672dc532c42dfc41c8ee04d21226))
* **micro:** 增加微应用控制[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([bebda01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bebda019b930f198da1d354480a723a29efdf047))
* **micro:** 增加微应用控制负责度[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([7e9fd5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7e9fd5fa3f4091a2298d0c35bbb5a25eb4a59673))
* **micro:** 增加微应用store的name属性[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0ef5917](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0ef5917ed71601dcc559709e6b62ee90e33f44a4))
* **micro:** 增加主应用标识判断 ([a71ee5d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a71ee5df08d2017d5adf2c198a096ac021dde80c))
* **micro:** 增加子应用激活路由配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([44ea958](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/44ea958233db96c2810d3a0a46619a441c0e44e4))
* **micro:** webpack打包主应用不处理output[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f3dc8b1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f3dc8b1dd5f3573fa4d4ce5efbb018ba42ca22c5))
* **mock:** 修复mock例子中的错误单词 ([c7f67f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c7f67f8fc7f9de63b6e76d8e5a469a084643993a))
* **modal:** 适配modal业务组件[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([d535089](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d535089b559effb197ef78d1d2071bd7d7e4a593))
* **monaco:** 去除monaco打包插件，通过静态资源引用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([dddb641](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/dddb641dd444d319df243ba5095be33057b54a3b))
* **mxgraph:** 修复源码，将所有属性注入到window，兼容沙箱模式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([44b6b3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/44b6b3ffcf8dfdf4a7df8389373d5b489795dbc5))
* **npm:** 更新pnpm-lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ab75426](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ab7542619055c2599fe514ae398d52248ebbdfe4))
* **package:** 更新所有包的依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f60ecd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f60ecd323b10533ad62bf689dff0c7aeb7a31a9b))
* **package:** 更新ant-design-vue版本[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([89efa95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/89efa95a824e42ecfb4cbc2203e6b53c2e32b24c))
* **package:** 更新libs包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2c42413](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2c424139741cc8f559bdb74937a78b5c8477c20f))
* **pinyin:** 修复pinyin-pro依赖引用问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ef71e00](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ef71e00761753aeba11233596c8dda6967d7d468))
* **plugin:** 增加plugin插件绑定JE[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0caaf66](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0caaf6687fd90a50f7829f8372e23695d837e99d))
* **proxy:** 只有在主应用才会启用子应用的调试代理地址 ([8def26b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8def26be5a7add2cc7a3996796bc0be75ac140ee))
* **route:** 修复子应用菜单获取路由数据问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([4a61aff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4a61aff54ea364a48a79b4e3831e65071bc1415a))
* **router:** 调整路由，路由守卫统一交由common管理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2bcc274](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2bcc27495ebc6f571a1827e89d29caa87ecd9c43))
* **router:** 支持系统路由自定义配置，支持路由菜单 ([bcb37f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bcb37f77b2076b9d23ccfaf1f50cacaa5c373af9))
* **settings:** 调整系统设置按钮 ([41a8e2c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/41a8e2c09c2562ba580ae62a177b25ace854bad6))
* **static:** 修复由于主题插件导致打包失败问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([4f80a7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4f80a7ecb794eeac3efde7ea4bbb8179b184ba76))
* **style:** 调整系统滚动条的样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2d849a5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2d849a583b44b82ba8ceea8b16a714b716c38448))
* **style:** 调整scroll，button样式 ([ba0aaca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ba0aaca540345aade047b83300d629598ba93dfe))
* **style:** 开发模式下微应用不加载样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([19e0bd8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/19e0bd88ac37002f2e8e3631cd10f7b255a91f84))
* **style:** 修改style文件引用路径[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e3df4bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e3df4bc77cdbd6444cc3f6d6b77cafbf0117ce6a))
* **style:** 优化ui库的资源位置 ([c2890e3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c2890e35fe680260c5623f41cbc95e7bf39b83d4))
* **style:** 增加ant tabs样式调整 ([bf61f69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bf61f694e5fdecbf033d239d6fd31e5aa24ba542))
* **style:** 组件包的样式引用修复 ([ef5fc45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ef5fc455408a91e5a42b0e4377b2cb2606baee1c))
* **system:** 系统的方法改用@jecloud/utils里的initSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([b00e589](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b00e58957e732a252f23e550e5b01139f79c0bd8))
* **system:** 修改系统变量请求接口 ([b80e4e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b80e4e0bfa3dfa9a2988d51aa34ad1a88385aebf))
* **theme:** 表格主题色调整[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2b80f27](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2b80f27e71f4b8082a1af54450b2905a14cee71d))
* **theme:** 调整表格主题色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ba82c8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ba82c8b22e7a46a851c39c939e340e2e892e9c6f))
* **theme:** 调整主题打包文件[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([b046aef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b046aefd0edd171fded0db61f33d7613d7eab051))
* **theme:** 将主题变量改为文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([2c587f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2c587f041935cff2ef7ed4b6b1cdb76284f5922a))
* **theme:** 确定主题支持方式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([cb8d445](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cb8d44556dde96e1ed7de7aa33f48f8ad4d9c028))
* **theme:** 修复代码构建主题文件目录错误 ([adf0bc4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/adf0bc472a68697dc338cc7beec2afbb19c18d9d))
* **theme:** 修复登录页的样式 ([5d416c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5d416c415d615fa1e66445d872a774d96c22435f))
* **theme:** 修复页面头部主题颜色 ([d9376c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d9376c060ce3cd383f53f39215e6238019be08a8))
* **theme:** 修复主题色样式 ([8383043](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/83830436cfb35570bf3a88667e269d86689db101))
* **theme:** 修复主题样式 ([a6fdef7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a6fdef7afcf7becc1b109ea669db9e87c0d16d15))
* **theme:** 修复子应用不设置主题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([90e4475](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/90e44752431eef8a828e509c540686f7ad0f2f06))
* **theme:** 修改系统主题默认字体色为[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)f3f3f ([c5f0e6d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c5f0e6d17fc12ae7ca4ae14ecbd8cbfcfaad7306)), closes [#3f3f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3f3f3)
* **theme:** 修改ant主题变量引用 ([c75eaa4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c75eaa4a87bf86f7e0362b2c2b5e28fc1a43b705))
* **theme:** 优化主题色选择组件 ([a645eff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a645eff862ac9a71a0ec2084a62e6ceb7620c2de))
* **ui:** 绑定项目publishPath ([13f7d13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/13f7d13cf32fdc156d5ac67e98039eea0e1aa1d1))
* **websocket:** 修复websocket监听方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0183ee6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0183ee689d59f085c209e764512f7f0e94638372))
* **welcome:** 修复欢迎页的背景色和字体色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2ab3122](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2ab3122ee23db703b8d40ed28eeab8da81980c91))
* **workflow:** 移除@jecloud/workflow,增加@jecloud/plugin[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e69cbe2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e69cbe20caa548085bcf74d28a83a911d4596673))


### Features

* **按钮样式修改:** 按钮样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([4472e6e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4472e6ee012d3b11c59b32d33cb78cfd70e598d9))
* **按钮只读操作:** 按钮只读操作[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([1435af7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1435af7557992f5d872106862cc23bb9e0376aec))
* **保存按钮连点优化:** 保存按钮连点优化[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([1c3a3e6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1c3a3e6110c73d1ae21fc23bd6b48d7cecf531f2))
* **保存操作处理:** 表单没有改变不触发保存操作[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([5e923ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5e923ffe979d9c568a2690304015d8dae8c65940))
* **保存级联操作:** 保存级联操作[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([08af14f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/08af14fc158a2d3e57c547915fd07937780a0bcd))
* **保存数据级联刷新:** 保存数据级联刷新[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([7bbd0ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7bbd0ff28e3699c05a06381795f29fd592e6db33))
* **保存刷新左侧菜单:** 保存刷新左侧菜单[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([0e3a50a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0e3a50ae05d4832bddc288a438005dbdcc29f8f8))
* **必填项统一封装:** 必填项统一封装[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([d5f3ef1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d5f3ef1fc19211a5d671ce672dd0caaef546f424))
* **编辑选中input:** 编辑选中input[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([fe62c6e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fe62c6e0470f3f10af87e8c2e85a76eaa123e7a9))
* **表单分页条个例替换:** 表单分页条个例替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([696003a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/696003aca425f7af64ba6cf2ff24d595eed635ac))
* **表单加正则校验:** 表单加正则校验[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f9432ce](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f9432ced495d4a97acfaea9c3732a6d7281409a0))
* **表单修改:** 表单修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([9edcf4c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9edcf4c68793edb76a73fb1e661b2e78ab71a3fe))
* **表单修改:** 表单修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([e7ec53e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e7ec53e0018df5d12ea6db4e0d1d61e4d42d1f8f))
* **表导出逻辑:** 表导出逻辑[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([8bd6292](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8bd62923ebb1b650a38c06be56792da6d026cb33))
* **表辅助:** 表辅助表单错误提示方式修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([a747825](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a747825c7bb512053314855924f8925960541f7c))
* **表辅助放出外键:** 表辅助放出外键[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([ce088d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ce088d3c92c65e982ea65afcf7afae087abba62a))
* **表辅助接口修改:** 表辅助接口修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([1e9d9d8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1e9d9d8cf9990ee187177463878e43487eaf2df5))
* **表辅助提交:** 表辅助提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([59ff73d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/59ff73d47836540d0c026a8f49c3445a693710e5))
* **表辅助提交:** 表辅助提交[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([fa004a8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fa004a831c7693513a3a75ddced7624ef737df87))
* **表辅助校验逻辑修改:** 表辅助校验逻辑修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([a3641e6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a3641e6dbb5b9bc8c92407cfebb2c67a73cbe413))
* **表辅助修改:** 表辅助修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8a2400f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8a2400f6645c02b3c0449a777c4370bc33679c49))
* **表格键,表格累主键类型禁用:** 表格键,表格累主键类型禁用[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([895e6b7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/895e6b726a3f85336f3e8c68314cebd888155c72))
* **表格键,表索引操作接口:** 表格键,表索引操作接口替换[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([d549b81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d549b8157ccfe6111366596f2f988eafa497f010))
* **表格键保存:** 表格键保存[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([5c79735](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5c79735f9b0460835721d750bf18e8fd12afdc80))
* **表格键表索引列表调整:** 表格键表索引列表调整[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([43a5e1f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/43a5e1f1b320971c3ede486b802492faa3c63e2b))
* **表格键分页:** 表格键分页[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([533bf4e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/533bf4e8707b4af5fc90df1abef99f882a58dacc))
* **表格键国际化:** 表格键国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([02deca9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/02deca9f2910840da4070fa11723592821123cca))
* **表格键删除:** 表格键删除[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8ba4429](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8ba4429dca18c80b9c94b0ceea87c0723170a295))
* **表格键添加操作:** 表格键添加操作[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([91bca6e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/91bca6e1ff1622b47d80a3fef50ead2fe0bf7308))
* **表格键修改:** 表格键修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([18ffac6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/18ffac6b0d03c59bee88a8b4ca50ce19b316edd8))
* **表格键修改:** 表格键修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([3956f01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3956f012fae3491907a535045bb4333a68ad746a))
* **表格键修改:** 表格键修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([9e8d0dd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9e8d0ddadd7404bb264ea75cb558901d0ce651c8))
* **表格键修改:** 表格键选资源表j_query参数修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([786b350](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/786b3500ec4735d8c8becf7f5c4255c4da887aa9))
* **表格键样式调整:** 表格键样式调整[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([5eada8e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5eada8ee6f5915070bf18ca40de82fa3ad340677))
* **表格键字段添加:** 表格键字段添加[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([62e3e64](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/62e3e645c62130ec61fd775b524235c5467d2e2d))
* **表格列保存:** 表格列保存[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([219c4fd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/219c4fd322ab4c8a17a7d30d893b7b73a414f4e3))
* **表格列操作接口提交:** 表格列操作接口提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([a5af945](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a5af9457b004b6ba9896bb3dd7880b207078ee3a))
* **表格列操作修改:** 表格列修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8f633ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8f633ae4d3dff5d1ae642f2add53f254dc7b1f4c))
* **表格列分页提交:** 表格列分页提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([ffd5498](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ffd5498fb1641650b64d6962a32dc9d211887005))
* **表格列国际化修改:** 表格列国际化修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([1c5ff16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1c5ff164f2ddd7bea52eb2a2682c22cfafeefe6f))
* **表格列接数据:** 表格列接数据[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([fddbcab](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fddbcab076433f2ed1b274463e4d5cf936fb63d1))
* **表格列排序:** 表格列排序[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([a31c52a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a31c52aadef4df5872b54e55e2bb54b09a4dd61e))
* **表格列删除保存:** 表格列删除保存l逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([bd5b94d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bd5b94d8aefd304f7701972595fd09d038b4f594))
* **表格列删除字段传参修改:** 表格列删除字段传参修改[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([d3e7387](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d3e7387491923d3ab9d0c57b90beb043d5ce13fe))
* **表格列样式修改:** 表格列样式修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([fb707be](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fb707beb37b1a0cecce9eeb08cf6ad11baf79a64))
* **表格列样式修改:** 表格列样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([a8ef98f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a8ef98fbcb7e65dd99b6b95d421bd4b0f007b57f))
* **表格列样式修改:** 表格列样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([8b531f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8b531f31a077b1d4e88ce03b537bc471735e360f))
* **表格列样式修改:** 表格列样式修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([13571ee](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/13571eeb465a0b721b94907a271551fa33e11ec3))
* **表格列主键修改:** 表格列主键修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([afec4f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/afec4f0d4736030e1397e452a409052644ef7002))
* **表格列:** 组件代码优化样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([408534e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/408534e9cde73d30d5b86a40f262871ad4c13a26))
* **表格列组件提交:** 表格列组件提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([b01f56c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b01f56cba78a6d3090bd22eb232c58acf0817921))
* **表格索引分页条:** 表格索引分页条[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([23cc3a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/23cc3a6a57972f81b1171f0f94e382ade0a2be74))
* **表格添加修改:** 表格添加修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([7be7c3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7be7c3f80db666f6d5c8288d8e0d3c22c5bbe4d5))
* **表名修改跟随:** 表名修改跟随[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f58ed0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f58ed0e617d12836ceb9fd5f3f4423064673d7d3))
* **表索引,历史留痕:** 表索引,历史留痕布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f4c004b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f4c004bf17d01844bd4a21ce2b0d1f6194ee1daf))
* **表索引保存:** 表索引保存[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([76a72b1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/76a72b1a5755339cee8e54f65cd8ea552fae3f93))
* **表索引不删除主键:** 表索引不删除主键[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([2b46383](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2b46383d80b071e705d9fcb323861389c018e918))
* **表索引国际化:** 表索引国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([d72a14b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d72a14bae382effc8b415a6260d85cd98768914b))
* **表索引列表布局编辑:** 表索引列表布局编辑[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([70af8f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/70af8f8258dbaaaaa7f5f62bac8f7db1dee8ce31))
* **布局调整:** 布局调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([6288565](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6288565f7610ddbb920269a8fd5b0a5032f3294a))
* **部门辅助:** 部门辅助面板提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([373a4e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/373a4e8e842f8e8e7d5931406fb4c9b911a3b431))
* **菜单图标参数修改:** 菜单图标参数修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([ea155ec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ea155eca98c68548e31c70249cc31465c5b0c0f1))
* **操作弹窗:** 操作弹窗代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([e04ebdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e04ebdd20b30bfa6ee355b99bad2a54fb53057c4))
* **测试数据:** 测试数据[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([1dd65cc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1dd65cc7a5622f1d93f30bb63d90ed2a4e7a822c))
* **查询接口提交:** 查询接口提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([048a70f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/048a70fddedef2c50db76436471d2495072b70ed))
* **查询选择参数修改:** 查询选择参数修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([7cdf0ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7cdf0ae542b1fba79b8d66d54feb3f64aed815d1))
* **查询选择排序:** 查询选择排序[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([e963bc3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e963bc37d70399c7eb852e6bd4d72ed257c7a6de))
* **查询选择树形选择:** 组件加title[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([559fc1f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/559fc1f7a7cefecfe1f03f22de6d250a65f83260))
* **查询选择修改:** 查询选择修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([3d5d5ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3d5d5ae78815777c1474af3944bc885266103e79))
* **查询选择修改:** 查询选择修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([9ec5de0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9ec5de048a9e3aec4301ed752471a37eda16c615))
* **查询选择修改:** 查询选择修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([84dce64](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/84dce64fb6dd8a3b31e6b6d491b5bf77c13f1ca3))
* **查询选择修改:** 查询选择修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([1d43367](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1d433671dcc377831340057f7b8b11fa910bdcf3))
* **查询选择修改:** 查询选择修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([0ccc174](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0ccc174189d5efda273c0bc7582d0be7100569ba))
* **查询选择组件替换:** 查询选择组件替换[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([c80e82a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c80e82a3f3fef7779ec98ea22cf9fb7f95ee6562))
* **产品菜单切换样式修改:** 产品菜单切换样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([f07e4eb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f07e4ebe80207f8368f9926e87fd510087f074b8))
* **产品接口:** 产品接口传参修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([50b183e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/50b183ebd616f5bb838fa3533a87fdd2c4a2340d))
* **产品切换修改接口参数:** 产品切换修改接口参数[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([f91784f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f91784f03472602ca4a0e2a00c02253337131d89))
* **产品说明:** 产品说明提示修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([db501e5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/db501e563197aa8392a4bbbc7c690e1cb328c43c))
* **产品修改接口传参:** 产品修改接口传参[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([3ae0ff3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3ae0ff33e64137da26312ddfaca3bafb9eb56445))
* **产品优化:** 产品优化[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([70c04e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/70c04e8acec05005c5b14f56fa204148bf048696))
* **创建功能窗口提交:** 创建功能窗口提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([82c1799](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/82c1799c161cf79df91ceb4f25ed6f8d0913953c))
* **创建功能加操作表名:** 创建功能加操作表名[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([aca5a39](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aca5a39854c792a47a20d535edc2c2bcfa22a570))
* **创建功能提交:** 创建功能提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([85c88ea](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/85c88eac6b38e9b63403ff3fb9bc5cdfadd479d5))
* **创建功能修改:** 创建功能修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2337d90](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2337d90430e9fcae1df44f5f3896021534df697d))
* **创建视图参数修改:** 创建视图参数修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([685639d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/685639dbb4a0ab377cb3463b046bb5c8f8ddc33c))
* **创建视图逻辑补全:** 创建视图逻辑补全[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([49403e6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/49403e63ae08969d47f023202ef35b8017dd817d))
* **创建视图:** 生成ddl逻辑修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([ec1b9d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ec1b9d95d8b6bd0c90ed4aff1f65424decc42004))
* **错误处理修改:** 错误处理修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([ff34ad4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ff34ad42581fcc093f439064f5ce1b4822e410b2))
* **错误提示修改:** 错误提示修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([1861f5c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1861f5caa7affee101b2dc585b56a593d15cc1fa))
* **错误消息提示修改:** 错误消息提示修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([16fb135](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/16fb1353177550a221158a7fde090c01ca2fd9e2))
* **打包文件提交:** 打包文件提价[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([b42c2c3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b42c2c3d33de0bb692c14f465a72b0ce47248448))
* **代码更新:** 代码更新[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([48f665c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/48f665cc2bbb8f10091cfb5541c4c418c5698f72))
* **代码还原:** 代码还原[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([8086262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8086262fed1acc5e6834748f91d1f224dfc71b86))
* **代码合并:** 代码合并[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([20a9418](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/20a9418121e34cec7c95d24b916c6a8fcc97abfc))
* **代码升级:** 代码升级[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([22cad90](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/22cad9041fe86a5c9177c7b98cf75d0606604a8b))
* **代码提交:** 代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([525d5e1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/525d5e118ea56ca26e2cd372d945673dc03887db))
* **代码提交:** 代码提交[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([597f20e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/597f20e4b2fe1b2614cea355a1a3c7df4b817ca3))
* **代码提交:** 代码提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([60b992f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/60b992f3e1f318ed44281b9e44c66175b11d58a1))
* **代码提交:** 代码提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([f3115cc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f3115cce681c42101e6b217f1f7cd17446da4b4a))
* **代码提交:** 代码提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([e6e8c39](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e6e8c39d4e9285340336f9d036450a23225b01c0))
* **代码提交:** 代码提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([46bda43](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/46bda43b4fb7a548e70ed7c3686e06c9e740534c))
* **代码提交:** 代码提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([16e7cf8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/16e7cf84a9d63ec11bd89b35411266dd9050b2ba))
* **代码提交:** 代码提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8367393](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8367393bc199e8e02b1535784c536bd678d7a5ce))
* **代码提交:** 代码提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([d20f12b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d20f12bd404a35223b1bd4e6bb9a218fa9f9bcfe))
* **代码优化:** 代码优化[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([4210df9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4210df98aeee793c60613054a8e3d58d4db64d08))
* **代码优化提交:** 代码优化提交[[#13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/13)] ([6e1699a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6e1699a74df3db3391d19211b9170677b71e72e6))
* **单表不配置关系也可以生成视图:** 单表不配置关系也可以生成视图[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([984ffb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/984ffb2f6dd8a72e472abfd039fda6f3b88e613e))
* **弹窗按钮方法修改:** 弹窗按钮方法修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([56fd1b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/56fd1b5555b3a8b1dd37ca338a40f12e1b6dbdfd))
* **弹窗按钮文字修改:** 弹窗按钮文字修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([96bf990](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/96bf990857aee8247887b98576cf317588324fba))
* **弹窗加确定按钮oding:** 弹窗加确定按钮oding[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([d2bc6c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d2bc6c106e29b76b77885814f7d4e195b53e7fb6))
* **弹窗加loading:** 弹窗加loading[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([df902a0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/df902a04ad810452add7c8c7782dcef83acccd8a))
* **弹窗提交:** 弹窗提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([ca2b3f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ca2b3f0f9aed8b868a7adc9a1f275a1b71095ba4))
* **弹窗修改:** 弹窗修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([e9f9a68](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e9f9a6808a4e44c4ecba0b71786c30d8b5dbdb9a))
* **弹窗修改:** 弹窗修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([5ba0d36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5ba0d36bacb4a48c5ad5fb2976630d4ccde03737))
* **弹窗修改:** 弹窗修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([c6d8128](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c6d81286e642c8effa044ac7db763283a7180cae))
* **弹框代码抽离:** 弹框代码抽离[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([49af7b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/49af7b538c183a5bd763a0a034c14f81b5e296d1))
* **弹框消息提醒提交:** 弹框消息提醒提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([20d0a90](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/20d0a90994408a635e57710659dff97d48e9bc79))
* **导出表:** 导出表代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([731129e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/731129e4aa90bea5dcfb1e3cbc3261f026706dca))
* **导出代码提交:** 导出代码提交[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([a516c30](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a516c30f7b909a62c6586a1ec8b7013f194e6ec0))
* **导出:** 导出逻辑补全[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([7c240ce](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7c240ced66a7779770d36ac8b6eae5bcc3423309))
* **导出文件:** 导出文件[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([78d297a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/78d297af19c636ec9e2d7db21accc2d806a3d4ed))
* **导出文件修改:** 导出文件修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([73cbcfb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/73cbcfb13faa945414d70efb382e805c03290df9))
* **导出sql:** 导出sql[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([ee03bda](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ee03bda46c1355dd70d473474908a41f5c00aab9))
* **导出sql:** 导出sql[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([d957336](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d957336c06509c0da36a4e7ddbd4d5b6766e8474))
* **导出sql:** 导出sql[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([916b0fe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/916b0fe3f6c85b9c946e2cd04c2a93c0c4856a68))
* **导入表:** 导入表弹窗页面调整[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([8dd7c87](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8dd7c87c5826aa1ac04a65e628b54d4d94488d7c))
* **导入表:** 对应的字段修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([8859612](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/88596122746476b581bd57be303ae486767d47b2))
* **导入表逻辑:** 导入表逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([5af55ea](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5af55ea9d68f606d51a176a8e3e30b3a7952c092))
* **导入表逻辑添加:** 导入表逻辑添加[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([80ee1c9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/80ee1c9ab145a38db8405f816666d262325e509a))
* **导入表逻辑修改:** 导入表逻辑修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([1787cc3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1787cc3edb63f4c47c9b1037b3ab4bccbf3aeeb3))
* **导入弹框提交:** 导入弹框提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([d29a650](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d29a650483ebabe84881fadc116f6617b4d69e6a))
* **导入的表列表列校验修改:** 导入的表列表列校验修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([03df115](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/03df1158c0ef04f375d1e15ccb662e495bd3ebcb))
* **点击应用按钮刷新关系视图:** 点击应用按钮刷新关系视图[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([3a11005](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3a11005fd5210033a0d813884fa6f1d822a7fdc2))
* **调整按钮的间距:** 调整按钮的间距[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([9ebe3bf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9ebe3bf6ae0c2d14b1eaa104e69b339f9106741e))
* **放出跟换主键名:** 放出跟换主键名[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([72cc86c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/72cc86c57909d79d74c9e721567c7a5512039e32))
* **废弃接口删除:** 废弃接口删除[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([7954201](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/79542019222d7c71e8f490446f753a07154926d2))
* **分页条:** 分页条代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([a736943](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a736943fc5d767da6d1f105799b68199fc3ac33b))
* **分页条替换:** 分页条替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([778cf24](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/778cf245cf3992665bdd51651442ed3134d023fd))
* **分页条替换:** 分页条替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([5e98eeb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5e98eeb8b437d67d8972fb2c55f26adca2bf9660))
* **分页条修改:** 分页条修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([5c35da3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5c35da344a7a3ef05f67ee4e6280b0a8eb7d09b4))
* **辅助字段修改:** 辅助字段修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([591bda1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/591bda190f77f0d1979b63e016654e3128bdbca0))
* **复制表:** 复制表加提示信息[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([2fe3c80](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2fe3c80420b1bdf36e90f996e1ca3b70516ee95f))
* **复制表模块:** 复制表模块代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([5039642](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/50396422e84146f6b15c5019e310a19431500add))
* **复制成功提示语:** 复制成功提示语[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([c32a3af](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c32a3afe496059dfa3a6df241a30da3c8fbf94ff))
* **复制传参修改:** 复制传参修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([c5dafc2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c5dafc280b514cf87bcb949aaa1661f060f60bb1))
* **复制传参修改:** 复制传参修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b7cff42](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b7cff4252590ec89a8845cfac63d7a9157be91ef))
* **复制SQL调整:** 复制SQL调整[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([815fc03](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/815fc032d7a84458a238778383e8d43b50c5aed2))
* **复制SQL提交:** 复制SQL提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([9bfb60a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9bfb60a126ad33bd01ad4fb86f1615c38f94bed7))
* **高亮搜索提交:** 高亮搜索提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([b569d75](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b569d752cebcff844c4272dfe95c7c1745a4eb7d))
* **更换主键名:** 更换主键名[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([45c348f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/45c348fcd436f96093afdb8803146b668b74369c))
* **更新代码:** 更新代码样式修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([7f934f4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7f934f4c7d1805a0d60194c3fc71486bdd50d05a))
* **公共接口修改:** 公共接口修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([6dcb6de](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6dcb6de1962e0dbc8d94f6ea1baf6d388a8d47fc))
* **关系设置联动:** 关系设置联动[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([7b54f50](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7b54f50639e36d390f65d95eca7fb6bf1542e09f))
* **关系视图点击:** 关系视图点击[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([aed8b02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aed8b026599f4cfb1cbd0d0cc46614b4b33451a1))
* **关系视图:** 关系视图页面提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([9234a1b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9234a1b206f13f76f29df7d1d5ccf57f40a21bba))
* **关系视图提交:** 关系视图提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([77285ea](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/77285eadeff12898c97d969bbe67693737a6faac))
* **关系视图:** 添加功能必填项逻辑优化[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([9037b1b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9037b1b5d39e141e30c5d0236aac95e40de3faa0))
* **关系视图添加功能加校验:** 关系视图添加功能加校验[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([2212142](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/221214279a137bc8405064b1b86770ca41a6fe5b))
* **关系视图修改:** 关系视图修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([e5c2f4f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e5c2f4f858dab30966f0dfcd77ce10e2cc362c8c))
* **关系视图:** 支持打开功能配置[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([3176ded](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3176ded1d5f58b2c134fe67df1c2b6cb242fec42))
* **国际化代码提交:** 国际化代码提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([23557b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/23557b55dd7a8b6662d735c43173ae73700e4825))
* **国际化提交:** 国际化提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([abc8a94](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/abc8a94b84e2647ba1ae5abbe4a08ce144158708))
* **国际化左侧树提交:** 国际化左侧树提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([ede31da](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ede31da45c4b1f985e304f056597fc77403a8340))
* **换接口名称:** 换接口名称[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([10c9740](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/10c97401483913546693e65184cabe2502a273c3))
* **级联字典辅助提交:** 级联字典辅助提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([099fa37](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/099fa371b6a22c469cb9e9ee99764eebf3f914b4))
* **加产品切换逻辑:** 加产品切换逻辑[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([0775faf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0775faf518d9f51a9d03fa7d68dba8077e38bc34))
* **接口跟换提交:** 接口跟换提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([c90b13a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c90b13a7b9a6331dce69a3b951169d9e4ab17388))
* **接口恢复:** 接口恢复[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([1ff6efe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1ff6efea842fbac2e4802389fa7ad37ba9256ad6))
* **接口名跟换:** 接口名跟换[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([a4d0f20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a4d0f200559ade7078b33b0a5db173fa09604b5b))
* **接口替换提交:** 接口替换提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([8698f1d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8698f1de408a3cccf6161b5d7f68acd6dd65c944))
* **警告修复:** 警告修复[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([23240cd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/23240cd9f59fee9e273c69c3de5f7303ff42b028))
* **历史记录国际化:** 历史记录国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([ee4747c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ee4747c4d90e3706ccd57325cdc5ab64b1d90df7))
* **历史留痕接口跟换:** 历史留痕接口跟换[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([6f6bd72](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6f6bd72aa54bd87c095499bd54b231fd8bc73b5e))
* **历史留痕:** 历史留列居中[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([728703f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/728703f36d2d0ae885491d4ae0aa7de421c5f22d))
* **历史留痕列表:** 历史留痕列表[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([75509de](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/75509de84501b256ced31fa0de5c8bd2adc3836f))
* **历史留痕下拉列表高度修改:** 历史留痕下拉列表高度修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([014c08c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/014c08c0bd7e5001ee721c4732688a88c169631c))
* **列编辑修改:** 列编辑修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([9ecc5b1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9ecc5b1a23174879064243d16ef16b367ed5d831))
* **列表边框样式修改:** 列表边框样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([fda3ecb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fda3ecbaf83896bf3e983571ade5e1c727583987))
* **列表编辑代码提交:** 列表编辑代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([a8517c6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a8517c63cd40cf551e964fb9f48dbeba55766aa8))
* **列表不要top提示:** 列表不要top提示[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([0cf4e4b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0cf4e4b030b5e930ba5f6554893034c21f4f3d8f))
* **列表分页条加刷新按钮:** 列表分页条加刷新按钮[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([8c37330](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8c37330a3b3f3c46003d18595888ed22a131a3bc))
* **列表加loding:** 列表加loding[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([d7da7da](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d7da7dab8ae787065a482c070d5df8cd5ca46906))
* **列表删除:** 列表删除逻辑优化[[#20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/20)] ([81d2d72](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/81d2d72a6fd80aa1b650d659ac2bf7c905f10aae))
* **列表数据接入:** 列表数据接入[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d988c96](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d988c96f37084dfa8be5b60bdc6cdac5983bccfc))
* **列表搜索提交:** 列表搜索提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([1a3fc68](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1a3fc68041dd42956c9529b982be20e8b867e5dc))
* **列表索引添加删除:** 列表索引添加删除[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([88174d1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/88174d17aa23039b487f5388938c5a8a4428d957))
* **列表添加到首位:** 列表添加到首位[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([adde278](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/adde27845e80260cf7603cd9edc4ac799dbf471c))
* **列表样式调整:** 列表样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([2285269](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2285269f1f030da7f68bddcced938c4a00846b5e))
* **列表样式修改:** 列表样式修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([4765e41](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4765e41fa5f37a048b6271ef16f0197ca5f8e9f9))
* **列表loading优化:** 列表loading优化[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([b6469eb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b6469eb1a4cbd7f8750b35bf35685fc035084573))
* **列修改:** 列修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([75efbdc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/75efbdc84ff1632f9657efc796bfff039783111f))
* **流程字段添加:** 流程字段添加[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([9a19a6b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9a19a6b38dcb0030c6b9b6fc6250dd1cee196a2c))
* **名称正则校验修改:** 名称正则校验修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([b2fcb5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b2fcb5f8af6be00f0a05f836ed75983fcc50acc9))
* **模块重命名:** 模块重命名[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([2402941](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2402941c8a61a48f2fdfa3b0bd4fdba3f87c22e0))
* **模块重命名:** 模块重命名代码提价[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([6b9542c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b9542cc4ad5e63e95c99a2a179e12a0e9a2ca60))
* **模块重命名:** 模块重命名代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([913ddd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/913ddd3afe048e5189223bd0baa2afb2061ef155))
* **判断框提交:** 判断框提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([4865abf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4865abfe09cc0e8cba2530cb2705c084af11b706))
* **配置修改:** 配置修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([181eae2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/181eae2f3de21d91e0d76db3c04aa8ef06243353))
* **批量删除提交:** 批量删除提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([35894d7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/35894d76f76c649f68ad8b6c548b5733ff30c0a3))
* **清空缓存，升级标记:** 清空缓存，升级标记逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([9bc4a0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9bc4a0ec1c79a45345419f3f0fbe3909f6ca61ba))
* **请求头修改:** 请求头修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([e68a755](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e68a755e76292160df71a2fd92cf4371c436ad3e))
* **请求头修改:** 请求头修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([4dbfec3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4dbfec34827120a017b1a369c24c70eb043384ad))
* **去掉升级标记按钮:** 去掉升级标记按钮[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([bf109d7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bf109d787344a2bfad5761ae2c8b68509025b651))
* **去掉console:** 去掉console[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([fc19f2a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fc19f2af1fa2a54b738ceb0974b2449080fef8e4))
* **人员部门辅助修改:** 人员部门辅助修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([9383b44](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9383b448f0ae1205843a255158308928e7d4794d))
* **人员辅助表单高度修改:** 人员辅助表单高度修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([050d333](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/050d3337d3bfcd8e53b543939dab3172b3c14cbd))
* **人员辅助加字段判重:** 人员辅助加字段判重[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([72c4f56](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/72c4f567bb5c45d5c46e1f98664e4f01f24771e2))
* **人员辅助逻辑补充:** 人员辅助逻辑补充[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([cebe6e9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cebe6e9744806efb38bf8b66fa15cde9c9fe1816))
* **人员辅助逻辑补齐:** 人员辅助逻辑补齐[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([2d69566](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2d69566993ff830cc59979a5b8be85aacb21b99f))
* **人员辅助:** 人员辅助面板提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8b2c5de](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8b2c5de26bc9cc587a90c6d730986c03b0ba4093))
* **人员辅助修改:** 人员辅助修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([d992ae6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d992ae67356a9f2cf795109b1f50ddc181c8910f))
* **人员辅助修改:** 人员辅助修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([87d06da](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/87d06da46612eaeaf2bad4a9bc44031e3d06f5ca))
* **人员辅助修改:** 人员辅助修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b5ef237](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b5ef2377132cc8ec0e47955aa21eb3f2d7d6f4c0))
* **删除表参数修改:** 删除表参数修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([3a25471](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3a254712da7f7abea0f381fcaae80eb364b58a00))
* **删除调整:** 删除调整[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([12af1a0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/12af1a07375d64130da530b75ef98d892bf92107))
* **删除清空表逻辑:** 删除清空表逻辑补齐[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([62c1a09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/62c1a099d168d5cfbf4c6e1cd6089f606b66d4cb))
* **删除刷新逻辑:** 删除刷新逻辑[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([f89a0a5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f89a0a597a83c5fa6f4dcba27972df5a0781e5fe))
* **设计视图布局:** 设计视图布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f18e272](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f18e272a46b4cb6ff189e56534023cc30670bff2))
* **设计视图布局完成:** 设计视图布局完成[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([5024784](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/50247842eca2acf475f2b942a9c7ff44436528d4))
* **设计视图:** 设计视图代码提价[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([831afd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/831afd34a3630af0acfb934158cbad2fe4690a72))
* **设计视图:** 设计视图加帮助说明[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([72b5434](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/72b54344186b9aef37f838492a56769a4cd5e7f1))
* **设计视图修改:** 设计视图修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([bb2f1ba](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bb2f1ba3a8a2efb0dbe16f5d547cc2b20410ca26))
* **设计视图修改:** 设计视图修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([a6a3fa1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a6a3fa15a247f8042c4362db3537ca1783c31c71))
* **设计视图修改:** 设计视图修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([f415bb7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f415bb7ff24594b7b16d649d57a186d2cdc79fbc))
* **设计视图样式提交:** 设计视图样式提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([a1631b6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a1631b651213323bf0bd133b74ae2874758f1b04))
* **视图报错修复:** 视图报错修复[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([be42750](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/be427505b49433d25abdf500518fa7bcf30b392c))
* **视图布局提交:** 视图布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([43e316e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/43e316e7c2590f2b93b05521685d2920b995b848))
* **视图操作修改:** 视图操作修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([ce78802](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ce78802f8108e2d167bd56139df9fdc4ec42829a))
* **视图查询修复:** 视图查询修复[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([4a47010](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4a47010e1c701298d5179839e7aa5c0e16cbb96d))
* **视图传递位置:** 视图传递位置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([10fdf45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/10fdf455ed38e822831a562b0d537c2dbd7a0bf9))
* **视图创建逻辑提交:** 视图创建逻辑提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([f668004](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f6680045ce49710ee1dda6b71601bf00d3accb66))
* **视图创建修改:** 视图创建修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([b930c7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b930c7e0dbcff67ba864ba3e910f3d6243dd20f6))
* **视图代码提交:** 视图代码提交[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([ec8779f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ec8779f389f686bcc75692d7b5dbadc75662b130))
* **视图代码优化:** 视图代码优化[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([a93b81f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a93b81f127e55b26928e4d4a97871acd78f30141))
* **视图弹窗修改:** 视图弹窗修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([7e30c09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7e30c09333e367ca53ab6c0d62b750bf67b9bb6a))
* **视图弹窗:** 样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([9a22703](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9a227038674bcf59bf305408b6c6fba486d75054))
* **视图的一些显隐控制:** 视图的一些显隐控制[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([3dc3d4c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3dc3d4c38818ab1b49cc188b7ea11c435da7a5b3))
* **视图调优:** 视图调优[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([50febef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/50febefc08eb7ef6d7f1a19ea9f729a6a6ee1e5d))
* **视图改变列表的高度:** 视图改变列表的高度[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0630bf3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0630bf34601a075f6108912b771f7ebcc2288a7b))
* **视图关系设计列表布局提交:** 视图关系设计列表布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8c0dd49](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8c0dd492f1ad40abb7382e3ac077bbb43f1870dd))
* **视图关系设置:** 视图关系设置逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8cfb869](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8cfb869b30dd8c26cac42655a90515e90f6f2586))
* **视图规划判空:** 视图规划判空[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([6fc2023](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6fc202362b312e13e8e190e3497e1b9c402e029e))
* **视图回显接口调试:** 视图回显接口调试[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([f5b0144](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f5b0144113853b251a7960d901412a347f2f78bd))
* **视图列表加过滤条件:** 视图列表加过滤条件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([7ae4324](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7ae43241d0e988122c50d6e19faca2ce701c9539))
* **视图列表去掉斑马线:** 视图列表去掉斑马线[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8e7de96](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8e7de96b47fc7bc3362e23a20ae127539e23d7f1))
* **视图列表样式修改:** 视图列表样式修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([ca1a414](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ca1a414ab4cac0a8e75ad2b6bbd40302593c14fc))
* **视图模块:** 视图模块添加删除逻辑[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([9ea0769](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9ea0769acdd2b55a4da88fb1f4a2abd133d606fa))
* **视图设计布局修改:** 视图设计布局修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([7dda8f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7dda8f760529f67b3aabf6a0aa4c16d88505a70e))
* **视图设计弹窗:** 样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([fcee112](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fcee11211755eafbf0700fbd57f94d58836136eb))
* **视图设计国际化:** 视图设计国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([a1131a1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a1131a1e8af5832db13b2099d8b12b622b3aa67a))
* **视图设计列表搜索:** 视图设计列表搜索[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([9375ae5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9375ae542a691791dc8ec55c8e4a40f6003d50c8))
* **视图设计列表选中:** 视图设计列表选中[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d6c05ad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d6c05add1f9e3eda11525ad98c35bb9b7ebb0922))
* **视图设计列表样式调整:** 视图设计列表样式调整[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([3202126](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3202126fd3d36c02685a426b1070894d78235785))
* **视图设计器:** 视图设计器布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([4f42476](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4f424765596849ff42d5cf4bce1472958c4668e0))
* **视图设计器:** 视图设计器代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([b66d64b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b66d64bf77f37d12780d23f9413fc35dffca718e))
* **视图设计器:** 视图设计器关联关系提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([e1c7998](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e1c7998d127b7263b80c65352672de914c548552))
* **视图设计器输出视图:** 视图设计器输出视图可编辑[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([1366226](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/13662267e4fd9e540eb4adb71ff0817ad351f602))
* **视图设计器提交:** 视图设计器提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f0405a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f0405a490f68ec48495f5d1476ae8e63d75b6765))
* **视图设计器修改:** 视图设计器修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([71d19e3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/71d19e396e8dc39d6f4697292af583b67b307c4f))
* **视图设计器修改:** 视图设计器修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([18fa7cb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/18fa7cbf1fe4c9064e760eeafc70584c74a89b13))
* **视图设计器修改:** 视图设计器修改{[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([d337d99](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d337d9954132221868386251a8510b9af1c1b8db))
* **视图设计:** 视图设计[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([1a366be](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1a366beed4f7918469018629548cbb2dc85c0e74))
* **视图设计提交:** 视图设计提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([c4e05d8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c4e05d81af01b49702c611c79026a6bf4fe4fdca))
* **视图设计提交:** 视图设计提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([de1834c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/de1834cf17fb5df620c680cfcd5ffb40a1c458f9))
* **视图设计选择器:** 视图设计选择器列表选中状态切换[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([fe2179d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fe2179d46da0819aae7db4f4d1a3bfdc86d2d82c))
* **视图设计样式修改:** 视图设计样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([6a68dc0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6a68dc07723654b3a27f2742c864ced0a1aee270))
* **视图设计页面:** 视图设计页面[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([8846a7b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8846a7bc417250941813be2592752a68f6da9dc8))
* **视图生成DDL修改:** 视图生成DDL修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([7a3fdb0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7a3fdb0511cc1d1b18a061c8918a5404782603bc))
* **视图生成sql:** 视图生成sql[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([8da2693](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8da2693dbc6b8e383ca39f76115eeda53e3beba3))
* **视图实时保存位置:** 视图实时保存位置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([05f93d8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/05f93d8bb9c1c3016610f5bfa18f9b51e73fa05a))
* **视图提交:** 视图提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([619bc87](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/619bc876d7b501c02efde71ffedadf717226e714))
* **视图为空提示:** 视图为空提示[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([24035f4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/24035f4bdc56e84965667c6efb8dad2c456742d5))
* **视图位置回显:** 视图位置回显[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([12a1e7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/12a1e7e7d073e0e64af0412047708c4da48c8aa6))
* **视图修改逻辑:** 视图修改逻辑[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([4755a66](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4755a6666ef5b51e288d249e51565616bd4b5672))
* **视图修改:** 视图修改[[#13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/13)] ([421cc6b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/421cc6b34d503e84cadf3a6cc6cf25911862bd9f))
* **视图修改:** 视图修改[][#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7) ([60b63b7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/60b63b723835374ace6efe08de59075ce598c5ac))
* **视图修改提交:** 视图修改提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([8e0f653](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8e0f653f8942b44f9bdb40cd3761fd3d7801bb1f))
* **视图修改重新生成DDL:** 视图修改重新生成DDL[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b86bb10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b86bb10ee10448cb06a0e3f08ce279c67a9242a5))
* **视图选中回显:** 视图选中回显[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([5caf7dc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5caf7dc91d31e250c43bd61b712a89d6a5da1c8e))
* **视图选中修改:** 视图选中修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([f2a13cd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f2a13cdc050ab0b295e0d67570c0d592912cc77a))
* **视图样式修改:** 视图样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([f69f387](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f69f387f6315f1de1ce77be4f3113f1bf15ceae4))
* **视图样式修改:** 视图样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([391bf71](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/391bf717bbc3001245b4b958f513e8b76d141251))
* **视图样式修改:** 视图样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([fda65d4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fda65d43dd0d86e134e09ef51e2e1c951626baf6))
* **视图样式修改:** 视图样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([783d4a5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/783d4a5f0fb03ed39c246315cbe4c7993730a74f))
* **视图样式修改:** 视图样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([ba73193](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ba73193e875fbc75bae46b29235ce93a5feca1f8))
* **视图样式修改:** 视图样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([992fa5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/992fa5fa4fcae0b4eefaf8e5f1ceee11c634afb0))
* **视图样式修改:** 视图样式修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([1dbbcf6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1dbbcf60f40e04f18267c60c8014dce56df68133))
* **视图支持排序:** 视图支持排序[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([e25f752](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e25f752b7266a17d2e8e58993093513434687a64))
* **视图字段替换:** 视图字段替换[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([694c53f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/694c53f13d984f1017003abeb12c40d1e3345ec2))
* **视图左侧树替换:** 视图左侧树替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([8b567d6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8b567d641ff0334c6ee798acbae7b313f6e21fde))
* **视图做显隐:** 视图做显隐[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([32bfa0b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/32bfa0b9b5e31a7d465491ff88db07a767f12215))
* **试图样式修改:** 试图样式修改[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([4b29b46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4b29b469a854e001bfca000b28192423fd533ac9))
* **树更多按钮图标修改:** 树更多按钮图标修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a0c08c8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a0c08c896cba855a223aa31372f2998709df9727))
* **树双击展开:** 树双击展开[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([d938749](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d93874911db6e46fae0fbafa053782f08bb35f83))
* **树搜索:** 树搜索展示code[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([9f860e9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9f860e9055acde6df9958da7e3449395b5f29000))
* **树拖拽代码提交:** 树拖拽代码提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([327147d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/327147dccfe12e583cbfc66ff9316bad40167c2f))
* **树形表调整:** 树形表调整[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([65c6e30](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/65c6e3032406b55f8837955daa4a7922d849ff1d))
* **树形拖拽接口参数修改:** 树形拖拽接口参数修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([aea4a98](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aea4a98076051ee0a8d7db74c012aba61c5e9057))
* **树形自定义图标:** 树形自定义图标[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([fc17301](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fc173010b510975552abfee84869f6f9167fb374))
* **树转移操作提交:** 树转移操作提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([ae575c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ae575c410bcea621b7c4d662dbbfca9da39ad305))
* **树组件拖拽完结:** 树组件拖拽完结[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([319684b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/319684b78774d1b15a9a524844ed8be5ee858a86))
* **数据留痕列表load:** 数据留痕列表load传参修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0017893](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/00178930615e0c021670d87c48e2c134e4fb2754))
* **搜索框高度修改:** 搜索框高度修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([ea9b662](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ea9b662e3dfdb7d5aebac67ae9fcfd90c4ff49fa))
* **搜索框样式修改:** 搜索框样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([5c4adc2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5c4adc2dbb0f5940415cf6accc885151e40b318c))
* **搜索框样式修改:** 搜索框样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([aca3714](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/aca37143a410a2f87934cbd145c6f21951b88ca1))
* **搜索逻辑修改:** 搜索逻辑修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([749f0e4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/749f0e4fa113972f414475e436738c04fcfafc63))
* **搜索添加删除按钮:** 搜索添加删除按钮[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([53cb320](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/53cb3207d29c6d3570947adf2db834df0de7e952))
* **索引删除调整:** 索引删除调整[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([1475ad6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1475ad6393fd17496cb0862b01a67f060f3c6742))
* **提交:** 提交[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([16e753e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/16e753eba998ef65563c197d332b1eb131f43a3c))
* **提示消息修改:** 提示消息修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([372081f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/372081fcb31c3de1752b3f8ab981e6247d6c6cc7))
* **添加表加产品ID:** 添加表加产品ID[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([409cb0f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/409cb0f849006cd15137fe5b599f795fa6276419))
* **添加模块弹框:** 添加模块弹框[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([be1db89](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/be1db89371c68f1c724b637f4bcd7ef0e4947921))
* **添加清空操作:** 添加清空操作[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([29a195c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/29a195cd696f3f5dcff8dc1e96aa60782ded2a86))
* **添加数据传icon:** 添加数据传icon[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([37f5682](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/37f5682b1333f64c9299bf965f5c1e4b92bf4ae8))
* **添加系统字段:** 添加系统字段[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d3e1a0a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d3e1a0a1e2d588b0be2a6d50687ee79005fca221))
* **同步按钮修改:** 同步按钮修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([b14c6bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b14c6bcbb5174f3b729bbaa040270da6f122b10e))
* **同步结构刷新数据:** 同步结构刷新数据[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([793c241](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/793c24168d49eb5de0e195039c57a5fe53413596))
* **图标提交:** 图标提交[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([54446f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/54446f784fcab792eea53934fb11d2e104a19d58))
* **图标替换升级:** 图标替换升级[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([5ce6bae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5ce6bae4a6919671eb73543dbe7963830dafb6ac))
* **图标替换:** 图标替换修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([4659c33](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4659c3303b18ddfe16561e658902808982adc522))
* **图标修改:** 图标修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([19c4a89](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/19c4a893b8a4decfa12217d2326e237f59cadca9))
* **拖拽事件调整:** 拖拽事件调整[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([c6b0d0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c6b0d0e818a462094a4181f609d5a80fdc3932a8))
* **完善正则校验:** 完善正则校验[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([afe83cf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/afe83cf5bdbfb208357205a69da558037fcbe013))
* **文本框组件修改:** 文本框组件修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([18b270d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/18b270dff07bce9ee072aa5dc442cf277641acfa))
* **下拉菜单单击弹出:** 下拉菜单单击弹出[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([9fa7b15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9fa7b15e2fbcfe0887d48d257bd8b5e79ffdaa0a))
* **下载文件后缀修改:** 下载文件后缀修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([6fc591f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6fc591f3e6c9fb1b66d488444a15661945dffbf0))
* **详情页面修改:** 详情页面修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b81050d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b81050d3e4c5f70d9c9bba5754b5fe32c5efdeaa))
* **消息提示修改:** 消息提示修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([441df31](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/441df3141416263fed491ddbe526fadb3d33eaa9))
* **修改默认标识:** 修改默认标识[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b2347de](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b2347de1df0bfd8fbe641b6193f5c17f80525d09))
* **修改视图数据刷新:** 修改视图数据刷新[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([0ef6249](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0ef624955ec9c1a923ed096c87226de731a6b4d2))
* **修改主键注释修改:** 修改主键注释修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([9ef096a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9ef096aa2ad4a1156d41d073c3c7db53784f9fca))
* **样式调整:** 样式调整[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([38b9d5c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/38b9d5c2ab191393294eeb380c6f272c5ecb4a9a))
* **样式提交:** 样式提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([1ff8a33](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1ff8a330ecd69d58b994c49f97614b247ad40144))
* **样式提交:** 样式提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([6644c13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6644c13301c89029de26b6b08e83a6c83409e83b))
* **样式提交:** 样式提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([e6a45e9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e6a45e9d8825c7d0a4c33ce4b2963d4c82cb542f))
* **样式提交:** 样式提交[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([7944a7b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7944a7b76bbb95dde4587d25bc489bcd45fb8a2e))
* **样式修改提交:** 样式修改提交[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([085d8c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/085d8c4091de0c0cece5d1eb20cd5a3cdaf95621))
* **样式修改提交:** 样式修改提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([e4547d8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e4547d88b4ae696e8caeb5dadaaec5b06636122d))
* **样式修改提交:** 样式修改提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([1824f32](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1824f32e3419e3b2e7add266ca96528087ea55b4))
* **样式修改:** 样式修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([59c1891](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/59c189187fd8cf933fdf5a67f29eb031aed59ba7))
* **样式修改:** 样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([2c2f2e4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2c2f2e4b127c2aa2f52cac751fe2e2b53927eb17))
* **样式修改:** 样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([8ee1fc2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8ee1fc2cbe48d96b0af0141e8a5d4af04c9da612))
* **样式修改:** 样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([f1f6f6f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f1f6f6fa272592b13993d5d57e7ece945462498b))
* **样式修改:** 样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([ffc2a56](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ffc2a56319f42472372a589803452bb588124f65))
* **样式修改:** 样式修改[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([9d7db51](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9d7db51cf887fa444098829beff431a77b97aea3))
* **样式修改:** 样式修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([fc407e2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fc407e2a4c1af2a00df1df82ed7c94e6842bcb79))
* **样式修改:** 样式修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([ea365bd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ea365bd9dd5e7ab2f5e06ce501f81d8e3364d800))
* **样式修改:** 样式修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([c04a4d6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c04a4d64c0777ecede9deeb1be70030c0c03c2e1))
* **样式修改:** 样式修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([db99b63](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/db99b63efb5bd2e0c94f10d1ea084458e38f2187))
* **样式修改:** 样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([468005a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/468005ad8b38bf857b475cc8697f633b5b7f9de3))
* **样式修改:** 样式修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([e4f0e09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e4f0e09d28c3828e0e14ab722c8a60cde6a33ab6))
* **样式修改:** 样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([9f2d77c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9f2d77c8a910a0b6850691968ada2212c06f25b6))
* **样式修改:** 样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([c91e1b9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c91e1b97821ab98c7e3046769165e4edda28fd11))
* **样式修改:** 样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([df22e03](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/df22e037815fb9b0489ae92dfd42fcb98d0a5bdc))
* **样式修改:** 样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([1199d16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1199d167d78b8b5f1fe58c4bf6ff3bd0bcc47eed))
* **样式修改:** 样式修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([447fb8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/447fb8b825d8001d557eb5eaf9bcbd209294a739))
* **依赖文件提交:** 依赖文件提交[[#13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/13)] ([c755e10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c755e10c4d7de5503d2a03c6a68b2941b6e387db))
* **依赖文件提交:** 依赖文件提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a2f4a7f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a2f4a7fed7cbde9d6f4deaaf2a654917a9c4f507))
* **应用表单修改:** 应用表单修改[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([c0db2d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c0db2d2a277538d4254e109b66a8ca147e831327))
* **应用表刷新左侧树:** 应用表刷新左侧树[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([e9019b4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e9019b46f18b507076e0f74ef665d10f9113dcd0))
* **应用操作修改:** 应用操作修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([3e78ccc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3e78ccc0fae7ebdda308d0b41106b7b97021cbbf))
* **应用操作修改:** 应用操作修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([43faf85](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/43faf85c48e700c70c41e57bda9003532a4bf64b))
* **应用操作优化:** 应用操作优化[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([a43df81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a43df813b0f63e7ae1e894286e95d716ff3b4c81))
* **应用防止多次点击:** 应用防止多次点击[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([5dd7008](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5dd700890f119b3e5345935cac9a506ce3cd6baf))
* **应用逻辑调整:** 应用逻辑调整[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([e4b07b3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e4b07b3cd912c2fd6e864c9fe8f2d9f47f400656))
* **应用逻辑:** 应用逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d7b35f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d7b35f77f8adf653d6ab86c0fd92c86a75ead953))
* **右键菜单加逻辑判断:** 右键菜单加逻辑判断[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([0fa893b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0fa893b29229bb544a20da9284e7cbc69ea16a6d))
* **整体样式调整:** 列表整体样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([3d3181e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3d3181e46633a9384bbe24289c116f3821ed82d9))
* **正则调整:** 正则调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([653bd8c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/653bd8cc8a5eb3fdbc9e88befbfa2ad439b73bff))
* **正则提示修改:** 正则提示修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([2f84a2d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2f84a2d4f3761d1eed888944285dc28b3b84758a))
* **智能排序修改:** 智能排序修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b775ccf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b775ccfc0ec2226c4290da623554d5887a2db6e0))
* **重命名逻辑提交:** 重命名逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([4869570](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/48695706c67831325ca32801f4adcd962a5d3376))
* **转移操作:** 加产品过滤[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([88855f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/88855f5f11d9c841e5c818c5e82c1458634ddbb4))
* **转移接口修改:** 转移接口修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([53fec26](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/53fec2607f32254ed8743e287f73c3b9aba3c7d4))
* **转移:** 转移弹窗代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([39c1b91](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/39c1b91010ef9a72b3f2bfbb1eabcf58661470b5))
* **资源表保存优化:** 资源表保存优化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([55bd061](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/55bd0611b1688771979f1162e7f7f32a31c4d4e2))
* **资源表:** 查询后台报错,列表loading取消[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([6e92597](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6e92597fc8a431e66432cc2df42f43a105e58f8b))
* **资源表调整:** 资源表调整[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([8b8a846](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8b8a846cfbd87041a0b09f98bba51ee43924512c))
* **资源表配置修改:** 资源表配置修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([09bd5d1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/09bd5d1ae9b178bd533c879d739eae22ed938004))
* **资源表提交:** 资源表提交[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([7685155](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/76851555042e02f0828f8b68cc15b7241dde71be))
* **资源表添加和删除:** 资源表添加和删除代码提交 ([1e28881](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1e288813a7734a8d407354a87fbc48a479212931))
* **资源表完善产品逻辑:** 资源表完善产品逻辑[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([2e8bf00](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2e8bf00017e7b786c4d6e5b26be52bc9bbb6c69c))
* **资源表修改提交:** 资源表修改提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([988e94a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/988e94a824579ee7a664e42277a33310c2de0560))
* **资源表修改:** 资源表修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([c2f8fd1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c2f8fd18a9bed8c1e57649803de1ae0be1398e99))
* **资源表样式修改:** 资源表样式修改[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([535a9e2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/535a9e21b095df73a461153eefd1e0320d4ce9fe))
* **资源表样式修改:** 资源表样式修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([eb78a0a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/eb78a0a2c0249df91087b157babf8f055a0e5292))
* **资源表转移优化:** 资源表转移逻辑优化[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([d8f0089](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d8f00895d20c5190c56f75479ca40c77bbcacbe2))
* **资源表:** 资源表左侧树代码提交 ([8de0845](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8de084571b6039afe302bb7c0e22d915e90a875d))
* **资源表左边四个tab:** 资源表左边四个布局提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d5bd86e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d5bd86e3ab97c4c435071f6759ab12876bf28c50))
* **字典辅助弹窗:** 字典辅助弹窗代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([f1e0e4e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f1e0e4e6af95b265850ad6454d5c48a1e6f4d81f))
* **字典辅助面板:** 字典辅助面板[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([594db3e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/594db3e82145073f6fb5856d08b24465aaaff05f))
* **字典辅助确定按钮校验:** 字典辅助确定按钮校验[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([a0a3b3d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a0a3b3def1f20d1af9efceed30c9fc9decdafacc))
* **字典辅助人员辅助修改:** 字典辅助人员辅助修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([6f3c9b9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6f3c9b9703cf29a512a430e58daa6146aa84b73b))
* **字典辅助校验:** 字典辅助校验[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([f4b991f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f4b991f3a2e8fa4081b4860f98628233cff1e3dc))
* **字典辅助修改:** 字典辅助修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([5976e60](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5976e60fcc1230745d574fcb0cf86731f8f792f1))
* **字典辅助修改:** 字典辅助修改[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([81b5c6e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/81b5c6e80654f4d080dfae956c0991d0836e1098))
* **字典辅助修改:** 字典辅助修改[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([cecb732](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cecb732f2fb13a8c2f2f37abc9013380836273f4))
* **字段辅助不可编辑:** 字段辅助不可编辑[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([32ca20b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/32ca20b07ba8cbc8f403775839c2b09f4a1f6b59))
* **字段辅助添加字段:** 字段辅助添加字段[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([eda17dd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/eda17dd106a6cf15b522272eb5b66ed9f9956fc2))
* **字段过滤修改:** 字段过滤修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/10)] ([32723e6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/32723e67de7514833f81fca7e65d7d67527ebe49))
* **字体图标:** 字段替换 ([8c21647](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/8c2164793f1dce9950e407b2ec289382022b9a82))
* **组件替换为je:** 组件替换为je[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([9b31c8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9b31c8b607142bcae7060071ea97f5b054a22cfe))
* **左侧树操作接口:** 左侧树操作接口替换[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([853fe36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/853fe367e5e0520a8d9f075893583987005e996b))
* **左侧树弹窗加国际化:** 左侧树弹窗加国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([b7f9ddc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b7f9ddc01cf6fb82f09272853a942e3366e68568))
* **左侧树加小点样式:** 左侧树加小点样式[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([8430354](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/84303547feace83a9800cfd5eeb9358a8b707596))
* **左侧树搜索选中修复:** 左侧树搜索选中修复[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([0ab28f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0ab28f606a728888fd92ac20a12467895737e2e4))
* **左侧树搜索选中:** 左侧树搜索选中并打开对应的表[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([1c366d4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1c366d4b8e6345ee8db85f942bcfbb968472333c))
* **左侧树:** 左侧树选中修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([f2b53ce](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f2b53ceb98f68f006fcc2856533738557754f8e0))
* **左侧主面板按钮显隐:** 左侧主面板按钮显隐[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([255697b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/255697b2a1ef921c5ed5dfb0e036cb063a641fad))
* **左侧tab布局实现:** 左侧tab布局实现 ([7be86fb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7be86fb57ac920844db83d5baf5d2eb0c08a64ab))
* **左侧tree:** 左侧tree代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([6e32b82](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6e32b8272cf222b692170eb30c6e476bdaa04466))
* **ajax:** 统一前后端数据格式 ([937fd40](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/937fd40adfcb81a15aecbf02087579fa56413a6f))
* **ant:** 更新antd版本3.0.0-beta.9 ([92a29e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/92a29e8b69eb7a5c9797aa377fca8c3feba0fd85))
* **antd:** ant-design-vue升级到3.1.0正式版 ([5cd3f38](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/5cd3f380bd898364663e2905d8a85e86c630d689))
* **base:** 项目基础目录结构搭建[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([2cdcfd8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2cdcfd8e19f423a6a86d4263f5a1ce3dd3cbf104))
* **build:** 增加@jecloud/workflow样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([9b66b09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/9b66b0909306d745bce60b9ab5170b7896d1eea5))
* **build:** 增加打包去除注释插件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([66282ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/66282ff1364b2d653d28f71e612f11c341ec612b))
* **build:** 增加微应用相关配置 ([137bc7f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/137bc7f5b5173a66a1b43a81301cb904a6fcd05d))
* **build:** 增加文件配置和路由守卫自定义入口 ([751b820](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/751b8203845909638919e2852ecfbf6e42eeb0a3))
* **ddl弹窗样式修改:** ddl弹窗样式修改[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/11)] ([ae04e61](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ae04e61f6cd518d0faf738221518687ec560e45c))
* **ddl面板完结:** ddl面板完结[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([cc38305](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cc383050e4723cbb4d8e3eda41b93f71413c07e0))
* **demo:** 调整入口文件，增加demo页面 ([889e3e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/889e3e000a616c05f67ba7b3ca34753d24557007))
* **demo:** 调整入口文件，增加demo页面 ([0d13adb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0d13adbb6a399296dca58d19550783214b0773c4))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([06b95f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/06b95f5c5dc6cd764587725e2d2f5265e7e5e420))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f412d5a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f412d5ac1bddeabf7859265f4facd41785e122b3))
* **doc:** 增加git操作技巧[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0eaf65d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0eaf65d1c1cda085057cc566b73f3a7accbb60ce))
* **doc:** 增加websocket使用说明[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([6f0d2ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6f0d2ca85dc4cd573ca59c7ef436c8cd13156fbc))
* **fonts:** 增加字体图标[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([6b01c9d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b01c9d118e0ce5422b0cfb0b6246c00cf00238c))
* **fonts:** 增加字体图标[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([6e87e81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6e87e81c1d19dd1905e5f522a75a515809912801))
* **form替换:** form替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([d59ec4c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d59ec4ccdc8343a34397b548e8ae791aa2331eb4))
* **func:** 增加jecloud/func功能包 ([2a24cb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2a24cb2ee1a9bae835b06043edf3fdbdd5f5e3da))
* **func:** useSystem增加功能操作函数[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/45)] ([62502c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/62502c12bcd9e077b57e5bc2b6e400978bac170d))
* **git修改:** git修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([966a1d1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/966a1d1162f1c00f2dd03aca4d8ef09d487af33a))
* **i18n:** 增加国际化支持[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([b9804df](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b9804dffbf89c2668b1b4ab15d462d90dbccbfaa))
* **i18n:** 增加国际化支持[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([cfcc144](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/cfcc1448f85c829dbcb46d01b9f575362d3ea376))
* **icon:** 增加icons路由，去掉icons.html[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([7accc7d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7accc7d1fea492d0cbbd86b47c609388409abe69))
* **icons:** 增加图标使用帮助[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([a508ca5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a508ca5c2ce61016aa51a2be93117a68a305bd92))
* **icons:** 增加图标使用帮助[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/4)] ([e7b5895](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e7b589560f172b718cd7be07b34f061d8b84fcdf))
* **icons:** 增加jeicons图标 ([37c1055](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/37c105591bb681c37725774048244ddfd085657b))
* **je:** 功能组件绑定JE对象，用于事件操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([4efc10c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4efc10cde1099b2e88d49717d960c628295ee376))
* **je:** 混入JE系统方法useSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([eb627a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/eb627a2e773b4a7d220260701a170e8703c9903c))
* **je:** 增加watchWebSocket方法，只有主应用可用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([20e0a0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/20e0a0e1a897fb2b1eb488eeb8cd51e794982033))
* **login:** 优化登录操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([45b29d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/45b29d24bba9930bee9a1cf6b9c5b41e83a6519d))
* **login:** login增加部门选择[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([770df7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/770df7ea8a47ce2881b06ed2c0bb2ff9fbfa074c))
* **micro:** 暴露micro-app的钩子函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([59e609d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/59e609dbc541d7e15f3e6a56dd347309c67a5cc4))
* **micro:** 调整微应用框架qiankun改为micro-app[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([23630f2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/23630f21e7c79f380806caf9ac8424e9f7f9b9c3))
* **micro:** 微应用支持触发其他微应用事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([0c54bf7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0c54bf77ae1733b1cccef5d2b45419bd31269b3b))
* **micro:** 增加主子应用通讯[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([2d7b3d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2d7b3d2fd72563184e8adadcc78abe90175521b2))
* **monaco:** 增加monaco静态资源，不再单独打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([c0961b0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/c0961b04dd161afc33a9e48a4baaf5e111888bee))
* **monaco:** 增加webpack，vite的monaco插件 ([1df85bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1df85bcf34dd7a68888de72026762dc6d05cf99d))
* **monaco:** 重写vite插件，修改路径引用错误问题 ([dea79f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/dea79f1790d42e4e896636112319ae9f4627b1e9))
* **mork:** 增加mork数据支持 ([3eb1d44](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/3eb1d449abfea042263f3d42f091c001d23138c7))
* **mxgraph:** 增加mxgraph静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([e4cdc85](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/e4cdc853b6db5d8f34ab2bd3c34a5ef1f07f766a))
* **panel布局修改:** panel布局修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2e10577](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2e105777bc5a048827d82fbc22831fcb6184aa23))
* **plan:** 增加globalStore的方案配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([a335bb5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a335bb5c9eeadcec12f55e17a97a1bb8ce0422b4))
* **preview:** 增加本地预览服务 ([0cd7aa1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/0cd7aa141fc756e900323fb142fe62d69271844f))
* **preview:** 增加本地ip输出，方便调试 ([4afdc69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/4afdc69fec00fbb0ff1daf85a368be9cec75c83d))
* **rbac:** 更新登录和获取用户接口[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([83a9597](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/83a9597c021c1c91e21f2e77a4614a675aaa0186))
* **router:** 支持用户自定义history ([1421921](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/142192104955f238e8112d959c1d55a35f1318fd))
* **router:** 支持自定义路由白名单[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([1ee13a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/1ee13a47dd250e2ea4ca2a3367d8bb455388a078))
* **sql查询传参:** sql查询传参修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([bfb1de3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bfb1de3075f572ff81f44bfb6b36d3eac71317ec))
* **SQL导出:** SQL导出[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([6b97072](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6b970725da2eb18b322da71c8e1a82efc1cd3afc))
* **sql列表国际化:** sql列表国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([abcd31a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/abcd31ae9c61853c4f6555902922c5fb9b63ad3d))
* **Sql列表国际化:** Sql列表国际化[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/5)] ([a5df663](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a5df663291f38dc8f9485e2113b7085626f74edf))
* **SQL视图查询:** SQL视图查询[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/2)] ([99ff107](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/99ff107169ae210ced705e6be39fc667b9caf5a5))
* **sql视图调整:** sql视图调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([6ec53e7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6ec53e702d982bc7c06c9ef37fe88a482611b1e0))
* **sql视图列显隐:** sql视图列显隐[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([52faa69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/52faa6925730a9f9d619aed9429794a630cbc602))
* **sql视图:** 面板逻辑提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([d8e45cd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/d8e45cd8cb76962cfd14d0e35320c7666e278978))
* **sql视图样式修改:** sql视图样式修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([7cec63a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7cec63ac758310fe4cb614ee9be3181f29b9a584))
* **sql修改:** sql修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([f7d94c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f7d94c47d298b533e2f07648f146ec5b1d7ea046))
* **sql转义:** sql转义[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/12)] ([7d9a1b4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/7d9a1b45ed2bc9f9872f74969e925c7fcfdfb128))
* **static:** 静态资源文件提取[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([f3e0edb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/f3e0edbbe6a20f33922449e56db057e37b777396))
* **static:** 增加静态资源包文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([28e62e5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/28e62e5b23fa933a368473623c2d152fad5e5d7a))
* **theme:** 增加灰色模式，弱色模式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([fa4d1df](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/fa4d1df85b69176cb5956e52ac77594b5f8307d2))
* **theme:** 增加主题支持[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([bbfa033](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/bbfa0333e212e51b73355b4bb948f41933c5064b))
* **theme:** 增加vben主题设置[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/6)] ([ad34b2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ad34b2ebcb496b8e422421bd940af99ab4666798))
* **theme:** 主题组件增加切换主题事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([ef9ac87](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/ef9ac87c6ae84c2f14de177d0ed89312ca7a0f30))
* **theme:** vuecli支持主题配置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/7)] ([b361a5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/b361a5f0e2e976884a25e4494d2fa003b8a583ce))
* **tinymce:** 增加tinymce插件，更新antd版本 ([13f390a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/13f390a11f4d131177ab94cde91ef694ace3a2a5))
* **toolbar布局修改:** toolbar布局修改[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/9)] ([df04d58](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/df04d58d8781cb256fbf1ff1a593737e17d01258))
* **tree操作修改:** tree操作修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([96aabc0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/96aabc05614890f14bdbb35af301a46b01794587))
* **tree加载返回值修改:** tree加载返回值修改[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/3)] ([6fe8185](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6fe81854c855fa3968810901373e527ff5e2bae2))
* **tree搜素代码提交:** tree搜素代码提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/1)] ([583af01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/583af013fccb9da73b0a34313ae0df5554c07363))
* **tree组件替换:** tree组件替换[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([2785c64](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/2785c645d3b4b68a42edbacf7cb34f26daee627b))
* **version:** release v1.0.0 ([53391a1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/53391a1dc67e12ae81f22f81d19a8c776ee97242))
* **version:** release v1.0.0 ([a969009](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/a96900905fea3ce679206313ddd57c2ec0504011))
* **workflow:** 增加@jecloud/worfkow[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/issues/8)] ([6d1aea5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-table/commit/6d1aea5c6719b6115ffe84a89c6f9b0c28861267))



