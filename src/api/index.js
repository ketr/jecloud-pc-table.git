/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_TABLE_GETTREE,
  API_TABLE_GETDATA,
  API_TABLE_GETCOLUMNDATA,
  API_TABLE_GETABLETKEYDATA,
  API_TABLE_GETABLEINDEXDATA,
  API_TABLE_GETABLEHISTORYDATA,
  API_TABLE_SELECTRELATION,
  API_TABLE_GETINFOBYID,
  API_TABLE_AddDATA,
  API_TABLE_DELETEMODULE,
  API_TABLE_ADDTABLE,
  API_TABLE_DELETETABLE,
  API_TABLE_COPYTABLE,
  API_TABLE_IMPORTTABLE,
  API_TABLE_SYNCTABLE,
  API_TABLE_MOVETABLE,
  API_TABLE_UPDATEDATA,
  API_TABLE_CLEARTABLECACHE,
  API_TABLE_ADDUPTAG,
  API_TABLE_APPLYTABLE,
  API_TABLE_VIEWCOLUMNCODE,
  API_TABLE_ADDCOLUMNBYDDLIST,
  API_TABLE_ADDCOLUMNBYDD,
  API_TABLE_DOSAVE,
  API_TABLE_GENERATECREATEINFO,
  API_TABLE_REMOVECOLUMN,
  API_TABLE_UPDATECOLUMNLIST,
  API_TABLE_DOSETPKNAME,
  API_TABLE_LOADTABLESQL,
  API_TABLE_LOADVIEWSQL,
  API_TABLE_SELECTONE,
  API_TABLE_LOADUSERTABLE,
  API_TABLE_ADDCOLUMNBYTABLE,
  API_TABLE_ADDTABLEKEYS,
  API_TABLE_DELETETABLEKEYS,
  API_TABLE_UPDATETABLEKEYS,
  API_TABLE_ADDTABLEINDEX,
  API_TABLE_DELETETABLEINDEX,
  API_TABLE_UPDATETABLEINDEX,
  API_TABLE_SELECTBYSHOWSQLVIEW,
  API_TABLE_GENERATEVIEWDDL,
  API_TABLE_SAVETABLEBYVIEW,
  API_TABLE_UPDATEVIEW,
  API_TABLE_DOWNLOADKEY,
  API_TABLE_DOUPDATE,
  API_TABLE_SAVEFUNCANDMENU,
  API_TABLE_GETFUNCINFOBYTABLE,
  API_TABLE_ADDTABLEASSISTCOLUMN,
  API_Board_Resource,
  API_TABLE_EXPORTBYSHOWSQLVIEW,
  API_TABLE_CHANGE_PRIMARY_KEYPOLICY,
  API_TABLE_IMPORT_LOADDATA,
  API_TABLE_IMPORT_TABLES,
  API_TABLE_GET_SQLVIEWSELECT,
  API_TABLE_DELETECHECK,
  API_TABLE_DO_TRANSLATE,
} from './urls';

/**
 * 获得左侧树的数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getTree(params) {
  return ajax({ url: API_TABLE_GETTREE, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得产品数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getData(params) {
  return ajax({ url: API_TABLE_GETDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得表格列数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getColumnData(params) {
  return ajax({ url: API_TABLE_GETCOLUMNDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得表格键数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getTableKeyData(params) {
  return ajax({ url: API_TABLE_GETABLETKEYDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得表格索引数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getTableIndexData(params) {
  return ajax({ url: API_TABLE_GETABLEINDEXDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得历史留痕数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getTableHistoryData(params) {
  return ajax({ url: API_TABLE_GETABLEHISTORYDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得历史留痕数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getRelationData(params) {
  return ajax({ url: API_TABLE_SELECTRELATION, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得表数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getInfoById(params) {
  return ajax({ url: API_TABLE_GETINFOBYID, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 添加数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addData(params) {
  return ajax({ url: API_TABLE_AddDATA, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除模块
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function deleteModule(params) {
  return ajax({ url: API_TABLE_DELETEMODULE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存表
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTable(params) {
  return ajax({ url: API_TABLE_ADDTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除表
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function deleteTable(params, headers) {
  return ajax({ url: API_TABLE_DELETETABLE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 复制表
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function copyTable(params) {
  return ajax({ url: API_TABLE_COPYTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 导入表
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importTable(params, headers) {
  return ajax({ url: API_TABLE_IMPORTTABLE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 同步结构
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function syncTable(params, headers) {
  return ajax({ url: API_TABLE_SYNCTABLE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 转移
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveTable(params, headers) {
  return ajax({ url: API_TABLE_MOVETABLE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 模块重命名
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateData(params) {
  return ajax({ url: API_TABLE_UPDATEDATA, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 清空缓存
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function clearTableCache(params, headers) {
  return ajax({ url: API_TABLE_CLEARTABLECACHE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 升级标记
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addUpTag(params) {
  return ajax({ url: API_TABLE_ADDUPTAG, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 应用
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function applyTable(params, headers) {
  return ajax({ url: API_TABLE_APPLYTABLE, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列添加字段判重
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function viewColumnCode(params) {
  return ajax({ url: API_TABLE_VIEWCOLUMNCODE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列级联辅助字典添加字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByDDList(params) {
  return ajax({ url: API_TABLE_ADDCOLUMNBYDDLIST, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列辅助字典添加字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByDD(params) {
  return ajax({ url: API_TABLE_ADDCOLUMNBYDD, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列字段添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTableColumn(params) {
  return ajax({ url: API_TABLE_DOSAVE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列使用新增字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addNewColumn(params) {
  return ajax({ url: API_TABLE_GENERATECREATEINFO, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列删除字段
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeColumn(params, headers) {
  return ajax({ url: API_TABLE_REMOVECOLUMN, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 表格列保存修改字段信息
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateColmunList(params) {
  return ajax({ url: API_TABLE_UPDATECOLUMNLIST, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 设置主键名
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updatePKName(params, headers) {
  return ajax({ url: API_TABLE_DOSETPKNAME, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * DDL视图查询sql(表)
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTableSql(params) {
  return ajax({ url: API_TABLE_LOADTABLESQL, params: params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * DDL视图查询sql(视图)
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadViewSql(params) {
  return ajax({ url: API_TABLE_LOADVIEWSQL, params: params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 人员辅助表单数据查询
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function selectOne(params) {
  return ajax({ url: API_TABLE_SELECTONE, params: params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 视图关系查询
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadUserTable(params) {
  return ajax({ url: API_TABLE_LOADUSERTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 人员辅助字段添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addColumnByTable(params) {
  return ajax({ url: API_TABLE_ADDCOLUMNBYTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表格键字段添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTableKeys(params) {
  return ajax({ url: API_TABLE_ADDTABLEKEYS, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表格键字段删除
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function deleteTableKeys(params, headers) {
  return ajax({ url: API_TABLE_DELETETABLEKEYS, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表格键字段修改
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateTableKeys(params) {
  return ajax({ url: API_TABLE_UPDATETABLEKEYS, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表索引字段添加
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTableIndex(params) {
  return ajax({ url: API_TABLE_ADDTABLEINDEX, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表索引字段删除
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function deleteTableIndex(params, headers) {
  return ajax({ url: API_TABLE_DELETETABLEINDEX, params: params, headers: headers }).then(
    (info) => {
      if (info.success) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 *
 * 表索引字段修改
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateTableIndex(params) {
  return ajax({ url: API_TABLE_UPDATETABLEINDEX, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * SQL视图查询
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function selectByShowSqlView(params, headers) {
  return ajax({ url: API_TABLE_SELECTBYSHOWSQLVIEW, params: params, headers: headers }).then(
    (info) => {
      if (info.success) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 *
 * 生成DDL操作
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function generateViewDdl(params) {
  return ajax({ url: API_TABLE_GENERATEVIEWDDL, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 创建视图
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveTableByView(params, headers) {
  return ajax({ url: API_TABLE_SAVETABLEBYVIEW, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 修改视图
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateView(params, headers) {
  return ajax({ url: API_TABLE_UPDATEVIEW, params: params, headers: headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 文件的fileKey
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function downloadKey(params) {
  return ajax({ url: API_TABLE_DOWNLOADKEY, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 通用修改数据的接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdate(params) {
  return ajax({ url: API_TABLE_DOUPDATE, params: params, headers: { pd: 'meta' } }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 创建功能
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveFuncAndMenu(params) {
  return ajax({ url: API_TABLE_SAVEFUNCANDMENU, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询表创建的所有功能
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getFuncInfoByTable(params) {
  return ajax({ url: API_TABLE_GETFUNCINFOBYTABLE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 表辅助添加数据接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addTableAssistColumn(params) {
  return ajax({ url: API_TABLE_ADDTABLEASSISTCOLUMN, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取资源表数据
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function GetResourceData() {
  return ajax({ url: API_Board_Resource }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 导出SQL文件
 * @export
 * @param {Object}
 * @return {Promise}
 */
export function exportByShowSqlView(params, headers) {
  return ajax({
    url: API_TABLE_EXPORTBYSHOWSQLVIEW,
    ...params,
    method: 'get',
    headers: headers,
  }).then((info) => {
    return info;
  });
}

/**
 * 模块重命名
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function changePrimaryKeyPolicy(params, headers) {
  return ajax({ url: API_TABLE_CHANGE_PRIMARY_KEYPOLICY, params: params, headers: headers }).then(
    (info) => {
      if (info.success) {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 加载导入列表数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getImportData(params, headers) {
  return ajax({ url: API_TABLE_IMPORT_LOADDATA, params: params, headers }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 导入表操作
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function importTables(params, headers) {
  return ajax({ url: API_TABLE_IMPORT_TABLES, params, headers }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 获得查询sql
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSqlViewSelect(params) {
  return ajax({ url: API_TABLE_GET_SQLVIEWSELECT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * 查询表是否可以被删除
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getDeleteCheck(params) {
  return ajax({ url: API_TABLE_DELETECHECK, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 *
 * sql翻译自然语言
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getTranslate(params) {
  return ajax({ url: API_TABLE_DO_TRANSLATE, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
