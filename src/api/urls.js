/**
 * API_URL命名规则：API_模块_方法
 */

//获得资源表树数据
export const API_TABLE_GETTREE = '/je/meta/resourceTable/getTree';

//获得产品数据
export const API_TABLE_GETDATA = '/je/meta/product/load';

//获得表格列数据
export const API_TABLE_GETCOLUMNDATA = '/je/meta/resourceTable/table/column/load';

//获得表格键数据
export const API_TABLE_GETABLETKEYDATA = '/je/meta/resourceTable/table/tableKey/load';

//获得表格索引数据
export const API_TABLE_GETABLEINDEXDATA = '/je/meta/resourceTable/table/tableIndex/load';

//获得历史留痕数据
export const API_TABLE_GETABLEHISTORYDATA = '/je/meta/resourceTable/loadHistoricalTraces';

//获得顶部表单的数据
export const API_TABLE_GETINFOBYID = '/je/meta/resourceTable/getInfoById';

//获得关系视图的数据
export const API_TABLE_SELECTRELATION = '/je/meta/resourceTable/selectRelation';

//添加模块数据
export const API_TABLE_AddDATA = '/je/meta/resourceTable/module/doSave';

//删除模块
export const API_TABLE_DELETEMODULE = '/je/meta/resourceTable/doClear';

//保存表
export const API_TABLE_ADDTABLE = '/je/meta/resourceTable/table/doSave';

//删除表
export const API_TABLE_DELETETABLE = '/je/meta/resourceTable/doHardDelete';

//复制表
export const API_TABLE_COPYTABLE = '/je/meta/resourceTable/table/doCopy';

//导入
export const API_TABLE_IMPORTTABLE = '/je/meta/resourceTable/import';

//同步结构
export const API_TABLE_SYNCTABLE = '/je/meta/resourceTable/table/syncTable';

//转移
export const API_TABLE_MOVETABLE = '/je/common/move';

//模块重命名
export const API_TABLE_UPDATEDATA = '/je/meta/resourceTable/doUpdate';

//清空缓存
export const API_TABLE_CLEARTABLECACHE = '/je/meta/resourceTable/clearCache';

//升级标记
export const API_TABLE_ADDUPTAG = '/je/meta/upgrade/addUpTag';

//应用
export const API_TABLE_APPLYTABLE = '/je/meta/resourceTable/table/apply';

//表格列添加字段判重
export const API_TABLE_VIEWCOLUMNCODE = '/je/meta/resourceTable/table/column/checkUnique';

//表格列级联字典辅助添加字段
export const API_TABLE_ADDCOLUMNBYDDLIST =
  '/je/meta/resourceTable/table/column/addAuxiliaryCasacdeDdColumn';

//表格列辅助字典添加字段
export const API_TABLE_ADDCOLUMNBYDD = '/je/meta/resourceTable/table/column/addAuxiliaryDdColumn';

//表格列字段添加
export const API_TABLE_DOSAVE = '/je/meta/resourceTable/table/column/doSave';

//表格列使用新增字段
export const API_TABLE_GENERATECREATEINFO = '/je/meta/resourceTable/table/column/addSystemColumn';

//表格列删除字段
export const API_TABLE_REMOVECOLUMN = '/je/meta/resourceTable/table/column/doDelete';

//表格列保存修改字段信息
export const API_TABLE_UPDATECOLUMNLIST = '/je/meta/resourceTable/table/column/doUpdateList';

//设置主键名
export const API_TABLE_DOSETPKNAME = '/je/meta/resourceTable/table/changeTablePrimaryKey';

//DDL视图查询sql(表)
export const API_TABLE_LOADTABLESQL = '/je/meta/resourceTable/table/showDdl';

//DDL视图查询sql(视图)
export const API_TABLE_LOADVIEWSQL = '/je/meta/resourceTable/view/showDdl';

//人员辅助表单数据查询
export const API_TABLE_SELECTONE = '/je/meta/resourceTable/selectOne';

//视图关系查询
export const API_TABLE_LOADUSERTABLE = '/je/meta/resourceTable/view/load';

//人员辅助字段添加
export const API_TABLE_ADDCOLUMNBYTABLE =
  '/je/meta/resourceTable/table/column/addAuxiliaryUserColumn';

//表格键字段添加
export const API_TABLE_ADDTABLEKEYS = '/je/meta/resourceTable/table/tableKey/doSave';

//表格键字段删除
export const API_TABLE_DELETETABLEKEYS = '/je/meta/resourceTable/table/tableKey/doDelete';

//表格键字段修改
export const API_TABLE_UPDATETABLEKEYS = '/je/meta/resourceTable/table/tableKey/doUpdateList';

//表索引字段添加
export const API_TABLE_ADDTABLEINDEX = '/je/meta/resourceTable/table/tableIndex/doSave';

//表索引字段删除
export const API_TABLE_DELETETABLEINDEX = '/je/meta/resourceTable/table/tableIndex/doDelete';

//表索引字段修改
export const API_TABLE_UPDATETABLEINDEX = '/je/meta/resourceTable/table/tableIndex/doUpdateList';

//SQL视图查询
export const API_TABLE_SELECTBYSHOWSQLVIEW = '/je/meta/resourceTable/table/selectByShowSqlView';

//生成DDL操作
export const API_TABLE_GENERATEVIEWDDL = '/je/meta/resourceTable/view/generateViewDdl';

//创建视图
export const API_TABLE_SAVETABLEBYVIEW = '/je/meta/resourceTable/view/saveTableByView';

//修改视图
export const API_TABLE_UPDATEVIEW = '/je/meta/resourceTable/view/updateView';

//文件的fileKey
export const API_TABLE_DOWNLOADKEY = '/je/meta/excel/downloadKey';

//通用修改数据的接口
export const API_TABLE_DOUPDATE = '/je/common/doUpdate';

//创建功能
export const API_TABLE_SAVEFUNCANDMENU = '/je/meta/resourceTable/table/saveFuncAndMenu';

//查询表创建的所有功能
export const API_TABLE_GETFUNCINFOBYTABLE = '/je/meta/resourceTable/table/getFuncInfoByTable';

//表辅助添加数据接口
export const API_TABLE_ADDTABLEASSISTCOLUMN =
  '/je/meta/resourceTable/table/column/addAuxiliaryTableColumn';

// 获取资源表首页模版数据（资源总数、类型分布、产品分布）
export const API_Board_Resource = '/je/meta/resourceTable/table/getHomePageInfo';

//导出SQL数据
export const API_TABLE_EXPORTBYSHOWSQLVIEW = '/je/meta/resourceTable/table/exportByShowSqlView';

// 保存主键生成策略
export const API_TABLE_CHANGE_PRIMARY_KEYPOLICY =
  '/je/meta/resourceTable/table/changePrimaryKeyPolicy';

// 加载导入列表数据
export const API_TABLE_IMPORT_LOADDATA = '/je/meta/resourceTable/metadata/tables';

// 导入表操作
export const API_TABLE_IMPORT_TABLES = '/je/meta/resourceTable/metadata/import/tables';

// 获得查询sql
export const API_TABLE_GET_SQLVIEWSELECT = '/je/meta/resourceTable/table/getSqlViewSelect';

// 查询表是否可以被删除
export const API_TABLE_DELETECHECK = '/je/meta/resourceTable/table/deleteCheck';

// sql翻译自然语言
export const API_TABLE_DO_TRANSLATE = '/je/meta/resourceTable/table/translate';
