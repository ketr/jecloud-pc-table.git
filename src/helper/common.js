export const verifyData = {
  name: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
      message: '不能输入非法字符',
    },
  ],
  code: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[A-Z]{1}[A-Z_0-9]{0,40}$',
      message: '编码由大写字母、下划线、数字组成，且首位为字母',
    },
  ],
  importCode: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[a-zA-Z]{1}[a-zA-Z_0-9]{0,40}$',
      message: '编码由字母、下划线、数字组成，且首位为字母',
    },
  ],
  lengthMax: {
    max: 40,
    message: '不能大于40个字符',
  },
  required: {
    required: true,
    message: '该输入项为必填项',
  },
};

export const JE_TABLE_PAGESIZEDATA = [
  {
    value: '10',
    lable: '10',
  },
  {
    value: '20',
    lable: '20',
  },
  {
    value: '30',
    lable: '30',
  },
  {
    value: '40',
    lable: '40',
  },
  {
    value: '50',
    lable: '50',
  },
  {
    value: '60',
    lable: '60',
  },
  {
    value: '70',
    lable: '70',
  },
  {
    value: '80',
    lable: '80',
  },
  {
    value: '90',
    lable: '90',
  },
  {
    value: '100',
    lable: '100',
  },
];

export const JE_TABLE_PAGESIZES = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

export const JE_TABLECLOUMN_SELECTOPTIONS = {
  typeOptions: [
    { value: 'ID', label: '主键' },
    { value: 'VARCHAR30', label: '字符串(30)' },
    { value: 'VARCHAR50', label: '字符串(50)' },
    { value: 'VARCHAR100', label: '字符串(100)' },
    { value: 'VARCHAR255', label: '字符串(255)' },
    { value: 'VARCHAR767', label: '字符串(767)' },
    { value: 'VARCHAR1000', label: '字符串(1000)' },
    { value: 'VARCHAR2000', label: '字符串(2000)' },
    { value: 'VARCHAR4000', label: '字符串(4000)' },
    { value: 'VARCHAR', label: '字符串' },
    { value: 'FOREIGNKEY', label: '外键' },
    { value: 'CUSTOMFOREIGNKEY', label: '外键(自定义)' },
    { value: 'NUMBER', label: '整数' },
    { value: 'DATE', label: '日期' },
    { value: 'DATETIME', label: '日期时间' },
    { value: 'FLOAT', label: '小数' },
    { value: 'FLOAT2', label: '小数(2)' },
    { value: 'YESORNO', label: '是非' },
    { value: 'CLOB', label: '超大文本' },
    { value: 'BIGCLOB', label: '巨大文本' },
    { value: 'CUSTOM', label: '自定义' },
    { value: 'CUSTOMID', label: '主键(自定义)' },
  ],
  treeTypeOptions: [
    { value: 'ID', label: '主键', disabled: true },
    { value: 'TEXT', label: '名称' },
    { value: 'CODE', label: '编码' },
    { value: 'PARENT', label: '父节点' },
    { value: 'NODEINFO', label: '节点信息' },
    { value: 'NODEINFOTYPE', label: '节点信息类型' },
    { value: 'NODETYPE', label: '节点类型' },
    { value: 'LAYER', label: '层次' },
    { value: 'ICON', label: '图标' },
    { value: 'DISABLED', label: '是否启用' },
    { value: 'NODEPATH', label: '树形路径' },
    { value: 'PARENTPATH', label: '父节点路径' },
    { value: 'HREF', label: '链接' },
    { value: 'HREFTARGET', label: '链接目标' },
    { value: 'ORDERINDEX', label: '排序字段' },
    { value: 'DESCRIPTION', label: '描述信息' },
    { value: 'NORMAL', label: '普通' },
    { value: 'TREEORDERINDEX', label: '树形排序字段' },
    { value: 'NONE', label: 'NONE' },
  ],
  isNullOptions: [
    { value: '1', label: '是' },
    { value: '0', label: '否' },
  ],
};

export const JE_TABLEkEYS_SELECTOPTIONS = {
  typeOptions: [
    { value: 'Primary', label: '主键' },
    { value: 'Foreign', label: '外键' },
    /* { value: 'Unique', label: '唯一' }, */
    { value: 'Inline', label: '内联外键' },
  ],
  lineTypeOptions: [
    { value: 'Setnull', label: '设置空' },
    { value: 'Cascade', label: '级联' },
    { value: 'Noaction', label: '不做处理' },
  ],
};

export const JE_VIEWDESIGN_SELECTOPTIONS = {
  formulaOptions: [
    { value: 'Sum', label: 'Sum' },
    { value: 'Avg', label: 'Avg' },
    { value: 'Min', label: 'Min' },
    { value: 'Max', label: 'Max' },
    { value: 'Count', label: 'Count' },
  ],
  orderOptions: [
    { value: 'asc', label: '升序' },
    { value: 'desc', label: '降序' },
  ],
  aMarkingOptions: [
    { label: '1', value: '1' },
    { label: 'n', value: 'n' },
    { label: 'left', value: 'left' },
    { label: 'right', value: 'right' },
    { label: 'on', value: 'on' },
  ],
  bMarkingOptions: [
    { label: '1', value: '1' },
    { label: 'n', value: 'n' },
    { label: 'left', value: 'left' },
    { label: 'right', value: 'right' },
    { label: 'on', value: 'on' },
  ],
  connOptions: [
    { value: '=', label: '等于' },
    { value: '>', label: '大于' },
    { value: '<', label: '小于' },
    { value: '>=', label: '大于等于' },
    { value: '<=', label: '小于等于' },
    { value: '!=', label: '不等于' },
    { value: 'LEFT JOIN', label: '左连接' },
    { value: 'RIGHT JOIN', label: '右连接' },
    { value: 'JOIN', label: '笛卡儿积' },
  ],
  typeOptions: [
    { value: 'ID', label: '主键' },
    { value: 'VARCHAR30', label: '字符串(30)' },
    { value: 'VARCHAR50', label: '字符串(50)' },
    { value: 'VARCHAR100', label: '字符串(100)' },
    { value: 'VARCHAR255', label: '字符串(255)' },
    { value: 'VARCHAR767', label: '字符串(767)' },
    { value: 'VARCHAR1000', label: '字符串(1000)' },
    { value: 'VARCHAR2000', label: '字符串(2000)' },
    { value: 'VARCHAR4000', label: '字符串(4000)' },
    { value: 'VARCHAR', label: '字符串' },
    { value: 'FOREIGNKEY', label: '外键' },
    { value: 'CUSTOMFOREIGNKEY', label: '外键(自定义)' },
    { value: 'NUMBER', label: '整数' },
    { value: 'DATE', label: '日期' },
    { value: 'DATETIME', label: '日期时间' },
    { value: 'FLOAT', label: '小数' },
    { value: 'FLOAT2', label: '小数(2)' },
    { value: 'YESORNO', label: '是非' },
    { value: 'CLOB', label: '超大文本' },
    { value: 'BIGCLOB', label: '巨大文本' },
    { value: 'CUSTOM', label: '自定义' },
    { value: 'CUSTOMID', label: '主键(自定义)' },
  ],
};
export const KEY_GENERATOR_TYPE = [
  { value: 'UUID', label: 'UUID' },
  { value: 'AUTO', label: '自增' },
  { value: 'AUTO_INCREMENTER', label: '序列' },
  { value: 'SQL', label: '自定义sql' },
  { value: 'NONE', label: 'NONE', disabled: true },
];
export const JE_TABLE_FIELD_SPECIFICATION = [
  { value: 'majuscule', label: '大写字母' },
  { value: 'lowercase', label: '小写字母' },
];
