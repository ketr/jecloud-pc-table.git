import { createApp } from 'vue';
import { setupCommon } from '@common/helper';
import micro from '@micro';
import lifecycle from './helper/lifecycle';

/* 系统文件，不做修改，请在lifecycle中进行业务处理 */
micro.setup((container) => {
  // Vue Init
  const vue = createApp(lifecycle.getApp());
  // VueInit Lifecycle
  return lifecycle
    .onVueInit(vue)
    .then(() => setupCommon(vue)) // Common
    .then(() => lifecycle.onVueBeforeMount(vue)) // VueBeforeMount Lifecycle
    .then(() => {
      // Mount
      vue.mount(container);
    });
});
